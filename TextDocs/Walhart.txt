	Planet Walhart is the center of a growing interplanetary empire.
This planet is rich in resources and is safe for travel, yet 
expensive to travel or live here. Mining rights are all but impossible
to obtain, only granted to those who prove their loyalties. The shops
carry perhaps a wider variety of items than any other planet in the
galaxy; they carry resources from every planet under its power and
influence. The police and armies are stronger here than anywhere else,
so you will be undoubtedly safe from any rogues. But one false move,
and it's off with your head here! A great place to find a mission to
help conquer the Galaxy! 	