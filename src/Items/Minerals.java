package Items;

import java.io.Serializable;
import java.util.ArrayList;

public class Minerals extends Resource implements Serializable {

	public Minerals(int itemTier) {
		super(itemTier);

		setValue();
		setName();
	}

	@Override
	public String getDescription() {

		String result = "";

		if (getName().equals("Chalk"))
			result += "COLOR: white "
					+ "LUSTER: porous."
					+ "HARDNESS: 0"
					+ "OTHER CHARACTERISTICS: Layers of chalk are known that are hundreds of feet thick, indicating extremely\n"
					+ "long periods of accumulation.";
		else if (getName().equals("Copper"))
			result += "COPPER: copper colored with weathered specimens tarnished green.\n"
					+ "LUSTER: metallic.\n"
					+ "HARDNESS: 2.5-3\n"
					+ "TRANSPARENCY: opaque.\n"
					+ "OTHER CHARACTERISTICS: ductile, malleable and sectile, meaning it can be pounded into other shapes,\n"
					+ " stretched into a wire and cut into slices.";
		else if (getName().equals("Diamond"))
			result += "HARDNESS: 10\n"
					+ "CLARITY: transparent\n"
					+ "OTHER CHARACTERISTICS: an excellent electrical insulator.The ultimate abrasive\n";
		else if (getName().equals("Flint"))
			result += " black, gray, brown, dark green or white.\n"
					+ "OTHER CHARARCTERISTICS: microscopic crystals of flint have a fibrous habit, which results in a notable behavior when chipped: the pieces are sharp, thin splinters called blades if large or flakes if small.\n";
		else if (getName().equals("Gold"))
			result += "COLOR:golden butter yellow.\n"
					+ "LUSTER: is metallic.\n"
					+ "TRANSPARENCY: ransparency is opaque.\n"
					+ "OTHER CHARACTERISTICS: almost indestructible and has been used and then reused for centuries to the extent that all gold of known existence\n"
					+ " is almost equal to all the gold that has ever been mined. Gold is a great medium metal for jewelry, as it never tarnishes.\n";
		else
			result += "Color is colorless\n"
					+ "Luster is vitreous\n"
					+ "TRANSPARENCY:crystals are transparent.\n"
					+ "OTHER CHARACTERISTICS: Only natural ice counts as a mineral such as snow, natural lake or river ice,"
					+ "glacieral ice and permafrost.\n";

		return result + " This mineral is worth " + this.value + " credits.";
	}

	@Override
	protected void setValue() {
		
		int[] valueByTier = { 1, 10, 15, 25, 40 };
		
		this.value = valueByTier[itemTier - 1];

	}

	@Override
	protected void setName() {
		
		ArrayList<String> nameOfMineral = new ArrayList<String>();
		nameOfMineral.add("Chalk");
		nameOfMineral.add("Copper");
		nameOfMineral.add("Ice");
		nameOfMineral.add("Gold");
		nameOfMineral.add("Diamond");
		this.name = nameOfMineral.get(itemTier - 1);

	}
}
