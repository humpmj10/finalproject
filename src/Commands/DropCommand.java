package Commands;

/**
 * Command to drop an item
 * @author Michael Humphrey
 *
 */
public class DropCommand extends TransactionCommand {

	private CommandReceiver cr;

	public DropCommand() {
		this.cr = cr.getHandle();
	}

	public void execute() {
		cr.methDropItem();
	}
}

