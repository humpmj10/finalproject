package Commands;

/**
 * Command that lists all the players that are currently logged into the game.
 * 
 * @author Michael Humphrey
 * 
 */
public class WhoCommand extends TransactionCommand {

	private CommandReceiver cr;

	public WhoCommand() {
		this.cr = cr.getHandle();
	}

	public void execute() {
		cr.methWho();
	}
}
