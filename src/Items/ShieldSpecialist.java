package Items;

import java.io.Serializable;

/**
 * The Shield Specialist is a Usable object whose effect is to replace a
 * player's equipped shield with a shield one tier higher. If already the
 * highest, will give the user a message.
 * 
 * @author Connor James Lewis
 * 
 */
public class ShieldSpecialist extends Usable implements Serializable {

	public ShieldSpecialist() {
		super();

		setName();
		setValue();
	}

	@Override
	public String getDescription() {

		return "You can \"use\" the Shield Specialist to permanently improve your shield by one tier. Hiring the specialist costs "
				+ this.value + " credits.";
	}

	@Override
	protected void setName() {
		this.name = "Shield Specialist";
	}

	@Override
	protected void setValue() {
		this.value = 55;
	}
}
