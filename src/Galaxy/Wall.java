package Galaxy;

import java.io.Serializable;

/**
 * 
 * @author Brent Murdock
 *This class represents the barriers in which a player nor MOB cannot move from room to room.
 *IMPORTANT: A section of wall can block ONE WAY! Note that it also blocks indiscriminately.
 */
public class Wall implements Serializable {
	
	/**
	 *  0    1   2   3   4   5   6
	 *  0_________________________
	 *   |   |   |   |   |   |   |
	 *  1_________________________
	 *   |   |   |   |   |   |   |
	 *  2_________________________
	 *   |   |   |   |   |   |   |
	 *  3_________________________
	 *   |   |   |   |   |   |   |
	 *  4_________________________
	 *   |   |   |   |   |   |   |
	 *  5_________________________
	 *   |   |   |   |   |   |   |
	 *  6_________________________
	 *   
	 *   Each part of the wall is not a single 7-7 2d array.
	 *   Part is a 7-6 array, the other 6-7.
	 *   Count the line segments in each section.
	 *   Some block east-west (6-7), some north-south (7-6).
	 *   
	 */
	
	private static Wall wall;
	
	private boolean[][] northWall;
	private boolean[][] southWall;
	private boolean[][] westWall;
	private boolean[][] eastWall;
	
	/**
	 * Creates the potential barriers.
	 * Constructor activates barrier around the edges, and the rest are inactive.
	 */
	private Wall(){
		northWall = new boolean[7][6];
		southWall = new boolean[7][6];
		eastWall = new boolean [6][7];
		westWall = new boolean[6][7];
		
		for(int i = 0; i < 7; i++){
			for(int j = 0; j < 6; j++){
				if(i == 0 || j == 0){
					northWall[i][j] = true;
					southWall[i][j] = true;
					eastWall[j][i] = true;
					westWall[j][i] = true;
				}
				else if(i == 6 || j == 5){
					northWall[i][j] = true;
					southWall[i][j] = true;
					eastWall[j][i] = true;
					westWall[j][i] = true;
				}
				else{
					northWall[i][j] = false;
					southWall[i][j] = false;
					eastWall[j][i] = false;
					westWall[j][i] = false;
				}
			}
		}
	}
	
	/**
	 * 
	 * @return Wall
	 * This is a singleton class.
	 */
	public static Wall getInstance(){
		if(wall == null){
			wall = new Wall();
		}
		return wall;
	}
	/**
	 * 
	 * @param d: Which direction the barrier is activated.
	 * @param row :What row
	 * @param col : What column
	 *  Toggles the barrier activation on or off.
	 *  BEWARE!!!!! Assumes valid input.
	 */
	public void turnBarriersOnOff(Direction d, int row, int col){
		if(d == Direction.NORTH){
			northWall[row][col] = ! northWall[row][col];
		}
		else if(d == Direction.SOUTH){
			southWall[row][col] = ! southWall[row][col];
		}
		else if(d == Direction.EAST){
			eastWall[row][col] = ! eastWall[row][col];
		}
		else{
			westWall[row][col] = ! westWall[row][col];
		}
	}
	
	/**
	 * 
	 * @param d
	 * @param row
	 * @param col
	 * @return: boolean
	 * Returns whether or not the wall is active in a given direction
	 * at a specific place. Assumes valid input.
	 */
	public boolean isActive(Direction d, int row, int col){
		if(d == Direction.NORTH){
			return northWall[row][col];
		}
		else if(d == Direction.SOUTH){
			return southWall[row][col];
		}
		else if(d == Direction.EAST){
			return eastWall[row][col];
		}
		else{
			return westWall[row][col];
		}
	}
}
