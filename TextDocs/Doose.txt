	The planet Doose is a very unspectacular environment, but it is a safe location 
for travelers. The government is protective of its citizens and visitors;
there will be no pirates or aliens to disturb you here. Police will attack if you break 
the law or attack other people here. 
	The downside is regulations are demanding on Doose. It costs a pretty penny 
to get a Visa here, and even more to get mining rights. The merchandise and
resources here are fairly standard, and you're not too likely to discover
rare minerals. What you're paying for to stay here is safety.	   
	