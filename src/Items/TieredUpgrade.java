package Items;

import java.io.Serializable;

/**
 * Most ship upgrades, e.g. shields, come in tiers; a lower-tiered upgrade is
 * not as effective as a higher-tiered one.
 * 
 */
public abstract class TieredUpgrade extends Upgrade  {

	// the upgrade's tier; 1 is lowest
	protected int tier;

	public TieredUpgrade(int itemTier) {
		super();

		tier = itemTier;
	}

	/**
	 * Returns this upgrade's tier as an integer ranking.
	 * 
	 * @return tier
	 */
	public int getUpgradeTier() {
		return tier;
	}
}