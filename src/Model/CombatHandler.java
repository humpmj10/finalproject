package Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import Items.*;

import ships.*;

/**
 * The CombatHandler class creates an object that mediates combat. This can be
 * between player vs player, player vs MOB, MOB vs MOB, and player vs planet
 * gov. This processes stats, determines loss and/or victory, and presents a
 * GUI-friendly summary of the battle.
 */
public class CombatHandler {

	private Controller controller = Controller.getInstance();
	private Ship theAttacker;

	public synchronized void attack(Ship attacker, Ship defender) {

		theAttacker = attacker;

		double attackerHealth = attacker.getStats().get("health");
		double attackerAttack = attacker.getStats().get("attack");
		double attackerDefense = attacker.getStats().get("defense");
		double defenderHealth = defender.getStats().get("health");
		double defenderAttack = defender.getStats().get("attack");
		double defenderDefense = defender.getStats().get("defense");

		if(defender instanceof Player) {
			controller.append((Player) defender, attacker.getName() + " attacks " + defender.getName()
			+ "!");
		}
		
		if (defender instanceof Player && attacker instanceof Mob
				&& defenderDefense * 2 < attackerAttack) {

			ArrayList<Item> inventory = ((Player) defender).getInventory();
			
			for (Item item : inventory) {
				if (item instanceof Fuel) {
					inventory.remove(item);
					controller.append((Player) defender, attacker.getName()
							+ " is too powerful for you! You expend a fuel tank in order escape his attack.");
					return;
				}
			}
		}

		boolean ongoing = true;

		Random rand = new Random();
		
		String report = "";
		
		while (ongoing) {
			rand = new Random();
			double odds = 30 - rand.nextInt(21);
			double damage = (attackerAttack / defenderDefense) * odds;
			damage = damage * 1000.0;
			Math.round(damage);
			damage = damage / 1000.0;
			defenderHealth = defenderHealth - damage;
			report += attacker.getName() + " strikes and does " + damage
					+ " damage to " + defender.getName() + "!\n";
			if (defenderHealth <= 0.0) {
				ongoing = false;
				report += attacker.getName() + " has won!\n";
				report += this.determineLoot(attacker, defender);
				if(attacker instanceof Player) {
					controller.append((Player) attacker, report);
				}
				if(defender instanceof Player) {
					controller.append((Player) defender, report);
				}
				break;
			}
			rand = new Random();
			odds = 30 - rand.nextInt(21);
			damage = (defenderAttack / attackerDefense) * odds;
			damage = damage * 1000.0;
			Math.round(damage);
			damage = damage / 1000.0;
			attackerHealth = attackerHealth - damage;
			report += defender.getName() + " counters and does " + damage
					+ " damage to " + attacker.getName() + "!\n";
			if (attackerHealth <= 0.0) {
				ongoing = false;
				report += defender.getName() + " has won!\n";
				report += this.determineLoot(defender, attacker);
				if(attacker instanceof Player) {
					controller.append((Player) attacker, report);
				}
				if(defender instanceof Player) {
					controller.append((Player) defender, report);
				}
				break;
			}
		}
		
		if (attacker instanceof Player) {
			ArrayList<Ship> fuzzbuster = attacker.getPlanet().getShipsOfType(
					Police.class);
			if (fuzzbuster.size() > 0) {
				((Police) fuzzbuster.get(0)).crimeWatch((Player) attacker,
						defender);
			}
		}

	}

	private String determineLoot(Ship winner, Ship loser) {

		if (theAttacker instanceof Player
				&& !(winner instanceof Pirate || winner instanceof Alien
						|| winner instanceof Thief || winner instanceof Police || winner instanceof Player)
				&& theAttacker != winner) {
			return "The pilot of the " + winner.getName()
					+ " warns you to stay away in the future.";
		}

		Item item = null;

		if (loser instanceof Player) {
			ArrayList<Item> loserInventory = ((Player) loser).getInventory();

			if (loserInventory.size() == 0) {

				int wallet = ((Player) loser).getWallet();

				if (wallet >= 15) {
					((Player) loser).subtractCurrency(10);

					return winner.getName() + " has seized 10 credits from "
							+ loser.getName() + " as loot!";
				} else {

					return loser.getName() + " drifts away in humiliation.";
				}
			}

			Random rand = new Random();

			int index = rand.nextInt(loserInventory.size());

			item = loserInventory.remove(index);

		} else {
			item = ((Mob) loser).getLoot();
		}

		if (winner instanceof Player) {
			if (loser instanceof Banker) {
				((Player) winner).addCurrency(50);
			} else if (loser instanceof Thief) {
				((Player) winner).addCurrency(10);
			} else {
				ArrayList<Item> winnerInventory = ((Player) winner)
						.getInventory();
				winnerInventory.add(item);
			}
		}

		return winner.getName() + " has won a " + item.getName() + " from "
				+ loser.getName() + " as loot!\n";
	}
}