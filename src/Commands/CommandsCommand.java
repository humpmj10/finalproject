package Commands;

/**
 * Executes a listCommands command.
 * 
 * @author Michael Humphrey
 * 
 */
public class CommandsCommand extends TransactionCommand {

	private CommandReceiver cr;

	/**
	 * Lists all the commands that are available
	 * 
	 */
	public CommandsCommand() {
		this.cr = cr.getHandle();
	}

	public void execute() {
		cr.methCommands();
	}
}
