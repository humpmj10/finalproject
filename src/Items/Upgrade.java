package Items;

import java.io.Serializable;

/**
 * A subclass of the Item superclass that covers items used to "upgrade" ships;
 * i.e., once they have been equipped, they affect certain stats or grant the
 * ship certain abilities.
 * 
 */

public abstract class Upgrade extends Item {

	public Upgrade() {
		super();

	}

	/**
	 * Gets a description of the upgrade's function, strength and value.
	 */
	public abstract String getDescription();
}