package Model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ships.*;
import Galaxy.*;
import Items.*;
import Networking.ClientList;
import Networking.Liason;

/**
 * This is main controller class for this game. It will be responsible for
 * moving players, around the galaxy, interactions between players. It will also
 * be responsible for issuing commands for the game. Also this class implements
 * the singleton pattern so that only one version of the game exists.
 * 
 * @author Michael Humphrey, Connor Lewis, Brian Smith, Brent Murdock.
 * 
 */
public class Controller implements Serializable {

	private Galaxy galaxy;
	private static Controller thisGame;
	private ClientList playerBase;
	private ConcurrentHashMap<Player, Liason> liasonsByPlayer;

	private Controller() {
		galaxy = Galaxy.getInstance();
		this.assignMobs();
	}

	/**
	 * Loads the player base from the save file
	 */
	public void loadPlayerBase() {

		this.playerBase = new ClientList();
		for (Planet p : Galaxy.getInstance().getUniverse()) {
			for (Ship s : p.getAllShips()) {
				if (s instanceof Player) {
					Player player = (Player) s;
					this.playerBase.addUser(player.getName(), player);
				}
			}
		}

	}

	/**
	 * Saves the player base to the save file
	 */
	public void savePlayerBase() {

		Galaxy.getInstance().saveGalaxy();
	}

	/**
	 * Sets hashmap.
	 * 
	 * @return
	 */
	public void setLiasonMap(ConcurrentHashMap<Player, Liason> lbp) {

		liasonsByPlayer = lbp;
	}

	public static synchronized Controller getInstance() {
		if (thisGame == null) {
			thisGame = new Controller();
		}
		return thisGame;
	}

	/**
	 * Moves the given player or mob from its current planet to the next
	 * specified planet.
	 * 
	 * @param ship
	 * @param next
	 * @param current
	 * @return true if move successful, false otherwise
	 */
	public synchronized boolean move(Ship ship, Planet next, Planet current) {

		// TODO: check whether legal move --> return false;

		current.removeOrbiter(ship);
		next.addOrbiter(ship);

		Planet planet = ship.getPlanet();

		return true;
	}

	/**
	 * Initializes all mobs, in their starting positions.
	 */
	private void assignMobs() {

		if (!Galaxy.getInstance().getPlanetsLoaded()) {
			// Laboratories: outside ring of planets
			this.newMob(2, new Laboratory("Sakharov Research Vessel",
					new int[] { 3, 4, 5, 11, 17, 23, 29, 35, 34, 33, 32, 31,
							30, 24, 18, 12, 6, 0, 1, 2 }));
			this.newMob(32, new Laboratory("Mobile Scientists Inc", new int[] {
					31, 30, 24, 18, 12, 6, 0, 1, 2, 3, 4, 5, 11, 17, 23, 29,
					35, 34, 33, 32 }));

			// Police: inner rings
			this.newMob(7, new Police("Unit Patrol", new int[] { 8, 9, 10, 16,
					22, 28, 27, 26, 25, 19, 13, 7 }));
			this.newMob(21, new Police("Vanguard Security", new int[] { 21, 20,
					14, 15, 21 }));

			// Philanthropist: innermost rings
			this.newMob(14, new Philanthropist("The Generous General",
					new int[] { 15, 21, 20, 14 }));

			// Pirates: throughout galaxy
			this.newMob(0, new Pirate("Dread Pirate Roberts", 5, new int[] { 1,
					2, 3, 4, 5, 4, 3, 2, 1, 0 }));
			this.newMob(5, new Pirate("Quantum Terror", 5, new int[] { 11, 17,
					3, 29, 35, 29, 23, 17, 11, 5 }));
			this.newMob(35, new Pirate("Molly", 5, new int[] { 34, 33, 32, 31,
					30, 31, 32, 33, 34, 35 }));
			this.newMob(30, new Pirate("Jack Sparrow", 4, new int[] { 24, 18,
					12, 6, 0, 6, 12, 18, 24, 30 }));
			this.newMob(6, new Pirate("Red Merc Cruiser", 4, new int[] { 7, 8,
					9, 10, 11, 10, 9, 8, 7, 6 }));
			this.newMob(29, new Pirate("Blue Merc Cruiser", 3, new int[] { 28,
					27, 26, 25, 24, 25, 26, 27, 28, 29 }));
			this.newMob(4, new Pirate("Black Merc Cruiser", 4, new int[] { 10,
					16, 22, 28, 34, 28, 22, 16, 10, 4 }));
			this.newMob(31, new Pirate("Yellow Merc Cruiser", 3, new int[] {
					25, 19, 13, 7, 1, 7, 13, 19, 25, 31 }));
			this.newMob(25, new Pirate("Violent Vincent", 3, new int[] { 25,
					26, 27, 28, 22, 16, 10, 9, 8, 7, 13, 19 }));
			this.newMob(20, new Pirate("Battered Can", 2, new int[] { 19, 25,
					26, 27, 28, 22, 21, 20 }));
			this.newMob(13, new Pirate("Rusty Tin", 2, new int[] { 7, 8, 9, 10,
					16, 15, 14, 13 }));
			this.newMob(20, new Pirate("Pesky Pat", 1, new int[] { 14, 15, 21,
					20 }));
			this.newMob(1, new Pirate("Brownbeard", 1, new int[] { 2, 3, 9, 15,
					21, 20, 19, 13, 7, 1 }));
			this.newMob(34, new Pirate("Rotten Wreck", 1, new int[] { 28, 22,
					16, 15, 14, 20, 26, 27, 33, 34 }));

			// Aliens: corners
			this.newMob(23, new Alien("Ferengi", 3, new int[] { 29, 35, 24, 35,
					29, 23 }));
			this.newMob(3, new Alien("Klingons", 2, new int[] { 4, 5, 11, 17,
					11, 5, 4, 3 }));
			this.newMob(18, new Alien("Reavers", 2, new int[] { 24, 30, 31, 30,
					24, 18 }));

			// Merchants: throughout
			this.newMob(28, new Merchant("The Capitalist", 4, new int[] { 22,
					16, 10, 4, 5, 11, 17, 23, 29, 35, 34, 33, 32, 31, 25, 26,
					27, 28 }));
			this.newMob(8, new Merchant("Thompson Storefront", 3, new int[] {
					9, 10, 4, 3, 2, 1, 0, 6, 12, 18, 24, 25, 19, 13, 7, 8 }));
			this.newMob(15, new Merchant("Flying Superstore", 3, new int[] { 9,
					10, 16, 22, 28, 27, 26, 25, 19, 13, 14, 8, 9, 15 }));

			// Engineers: central-ish
			this.newMob(13, new Engineer("Flying Chop Shop", new int[] { 7, 8,
					9, 10, 16, 15, 14, 13 }));
			this.newMob(22, new Engineer("Buddy's Garage", new int[] { 27, 27,
					26, 25, 19, 20, 21, 22 }));
			this.newMob(14, new Engineer("Humphrey Garage", new int[] { 15, 21, 20, 14 }));

			// Freighters: central-ish
			this.newMob(26, new Freighter("Cargo Hauler", new int[] { 25, 19,
					13, 7, 8, 9, 10, 16, 22, 28, 27, 26 }));
			this.newMob(20, new Freighter("Civilian Transport", new int[] { 21,
					15, 14, 20 }));

			// Banker & escort
			this.newMob(31, new Banker("Stellar Financial", new int[] { 30, 24,
					18, 12, 6, 0, 1, 2, 3, 4, 5, 11, 17, 16, 10, 9, 8, 7, 13,
					19, 25, 26, 27, 28, 22, 23, 29, 35, 34, 33, 32, 31 }));
			this.newMob(31, new Police("Stellar Security", new int[] { 30, 24,
					18, 12, 6, 0, 1, 2, 3, 4, 5, 11, 17, 16, 10, 9, 8, 7, 13,
					19, 25, 26, 27, 28, 22, 23, 29, 35, 34, 33, 32, 31 }));

			// Thief: throughout
			this.newMob(14, new Thief("Thieving Bastard", new int[] { 15, 21,
					20, 26, 27, 28, 22, 16, 10, 9, 8, 7, 6, 0, 1, 2, 3, 4, 5,
					11, 17, 23, 29, 35, 34, 33, 32, 31, 30, 24, 25, 19, 18, 12,
					13, 14 }));

			Galaxy.getInstance().setPlanetsLoaded(true);
		}
	}

	/**
	 * Helper method for constructing Mob list.
	 */
	private void newMob(int PlanetIndex, Mob mob) {
		galaxy.getPlanetByIndex(PlanetIndex).addOrbiter(mob);
	}

	/**
	 * This method interprets some basic textual inputs from the user such as
	 * look, engage, etc, and determines their validity. If they are valid, the
	 * relevant action is performed (with help from private methods for each of
	 * the commands). If the beginning of the command's argument is valid, it
	 * will parse, even if the argument trails off into nonsense.
	 * 
	 * @param player
	 * @param command
	 */
	public synchronized void readCommand(Player player, String command) {

		command = command.toLowerCase();

		Scanner sc = new Scanner(command);
		String first = sc.next();

		if (first.equals("look")) {
			if (!sc.hasNext()) {
				this.look(player);
			} else if (sc.next().trim().equals("at")) {
				if (sc.hasNext()) {
					String arg = sc.nextLine().trim();
					System.out.println("arg after \"look at\"" + arg);
					this.lookAt(player, arg);
				} else {
					append(player,
							"You must choose who or what you want to look at.");
				}
			} else {
				append(player, "That command is not valid!");
			}
		} else if (first.equals("move")) {
			if (!sc.hasNext()) {
				append(player,
						"You must choose a direction in which you want to move.");
			} else {
				this.move(player, sc.nextLine().trim());
			}

		} else if (first.equals("inventory")) {
			this.inventory(player);

		} else if (first.equals("engage")) {

			if (!(sc.hasNext())) {
				append(player,
						"You must specify who you want to interact with.");
			} else {
				this.engage(player, sc.nextLine().trim());
			}
		} else if (first.equals("attack")) {
			if (!(sc.hasNext())) {
				append(player, "You must specify who you want to attack!");
			} else {
				this.attack(player, sc.nextLine().trim());
			}
		} else if (first.equals("use")) {
			if (!(sc.hasNext())) {
				append(player, "You must specify what item you want to use.");
			} else {
				this.use(player, sc.nextLine().trim());
			}
		} else if (first.equals("drop")) {
			if (!(sc.hasNext())) {
				append(player, "You must specify an item to drop!");
			} else {
				this.drop(player, sc.nextLine().trim());
			}
		} else if (first.equals("get")) {
			if (!(sc.hasNext())) {
				append(player, "You must specify an item to pick up!");
			} else {
				String input = sc.nextLine().trim();

				if (input.contains("from")) {

					Pattern p = Pattern.compile("([a-z ]*) from ([a-z 1-9]*)");
					Matcher m = p.matcher(input);

					if (m.matches()) {
						String item = m.group(1);
						String ship = m.group(2);

						this.get(player, item, ship);
					} else {

						append(player,
								"You need to accurately specify both the item you want and who you want it from.");
					}
				} else {
					this.get(player, input);

				}
			}

		} else if (first.equals("give")) {
			if (!(sc.hasNext())) {
				append(player, "You must specify an item to give!");
				return;
			} else {
				String input = sc.nextLine().trim();

				if (input.contains("to")) {

					Pattern p = Pattern.compile("([a-z ]*) to ([a-z 1-9]*)");
					Matcher m = p.matcher(input);

					if (m.matches()) {
						String item = m.group(1);
						String ship = m.group(2);

						this.give(player, item, ship);
					} else {

						append(player,
								"You need to accurately specify both the item you want and who you want it from.");
						return;
					}
				} else {
					append(player,
							"You must enter \"give <item> to <ship>\" if you want to give an item.");
					return;
				}
			}
		} else {
			append(player, "\"" + command + "\" is not a valid command!");
		}
	}

	/**
	 * Returns a string representation of the player's inventory, including
	 * currency.
	 * 
	 * @param player
	 */
	private synchronized void inventory(Player player) {

		String output = player.getName() + "'s Inventory: \n";
		ArrayList<Item> inventory = player.getInventory();

		for (int i = 0; i < inventory.size(); i += 2) {
			Item item1 = inventory.get(i);
			output += item1.getName();
			if (i + 1 < inventory.size()) {
				Item item2 = inventory.get(i + 1);
				output += "\t\t" + item2.getName() + "\n";
			}
		}

		output += "\n" + player.getName() + "'s Equipped Items: \n";
		ArrayList<Item> equipped = player.getEquipped();

		for (int i = 0; i < equipped.size(); i += 2) {
			Item item1 = equipped.get(i);
			output += item1.getName();
			if (i + 1 < equipped.size()) {
				Item item2 = equipped.get(i + 1);
				output += "\t\t" + item2.getName() + "\n";
			}
		}

		output += "\nWallet: " + player.getWallet() + " credits.";

		append(player, output);
	}

	/**
	 * Takes the string command and parses it to a direction. If it is a
	 * legitimate direction and there is a planet in that diretion, the player
	 * moves to that planet; if not, error messages are sent.
	 * 
	 * @param player
	 * @param way
	 * @return
	 */
	private synchronized void move(Player player, String way) {
		if (way.length() < 4) {
			append(player, "You must choose a direction in which to move.");
		} else {
			// must make sure it can be parsed: east and west have 4 letters,
			// north and south have 5
			way = way.toUpperCase();
			way += " ";
			way = way.substring(0, 5).trim();

			if (way.equals("NORTH") || way.equals("SOUTH")
					|| way.equals("EAST") || way.equals("WEST")) {

				Direction direction = Direction.valueOf(way);
				Planet currentPlanet = player.getPlanet();
				Planet nextPlanet = currentPlanet.getNeighbor(direction);

				if (nextPlanet != null) {
					this.move(player, nextPlanet, currentPlanet);
				} else {
					append(player, "There is no planet in that direction.");
				}

			} else {
				append(player, "That is not a valid direction.");
			}
		}
	}

	/**
	 * A player uses an item in their inventory.
	 * 
	 * @param player
	 * @param itemToUse
	 */
	private synchronized void use(Player player, String itemToUse) {

		ArrayList<Item> inventory = player.getInventory();

		Item usableItem = null;
		for (Item item : inventory) {
			if (itemToUse.startsWith(item.getName().toLowerCase())) {
				usableItem = item;

				player.useItem(usableItem);
				return;

			}
		}

		this.append(player, "You do not have that item in your inventory!");
	}

	/**
	 * A player takes an item from a Mob or other player. This requires some
	 * back-and-forth; the only Mob that will comply is the Merchant (in which
	 * case this instigates the "purchase" method). If from a player, some
	 * back-and-forth communication is required to guarantee permission.
	 * 
	 * @param player
	 * @param itemName
	 * @param shipName
	 */
	private synchronized void get(Player player, String itemName,
			String shipName) {

		ArrayList<Item> myInventory = player.getInventory();
		Ship otherShip = null;
		Item parcel = null;

		ArrayList<Ship> ships = player.getPlanet().getAllShips();
		for (Ship ship : ships) {
			if (shipName.equals(ship.getName().toLowerCase())) {

				otherShip = ship;

				if (otherShip instanceof Player) {

					ArrayList<Item> otherInventory = ((Player) otherShip)
							.getInventory();

					boolean found = false;

					for (Item item : otherInventory) {
						if (itemName.startsWith(item.getName().toLowerCase())) {
							parcel = item;
							found = true;
							break;
						}
					}

					if (!found) {

						append(player, otherShip.getName()
								+ " doesn't even have that item!");
						return;
					}

					append((Player) otherShip,
							"Could you give me a " + parcel.getName() + "?");

					otherInventory.remove(parcel);
					myInventory.add(parcel);

					return;

				} else if (otherShip instanceof Merchant) {

					player.purchase(((Merchant) otherShip), itemName);

					return;
				}
			}
		}

		if (otherShip != null) {
			append(player, otherShip.getName()
					+ " isn't giving you squat, bucko!");
		} else {
			append(player, "That ship isn't in orbit!");
		}
	}

	/**
	 * A player gives an item to another player or a merchant. If to another
	 * player, it is automatically placed in his/her inventory; if a merchant,
	 * then the player "sells" the item and receives a profit.
	 * 
	 * @param player
	 * @param item
	 * @param shipName
	 */
	private synchronized void give(Player player, String itemName,
			String shipName) {

		ArrayList<Item> myInventory = player.getInventory();
		Ship otherShip = null;
		Item parcel = null;

		for (Item item : myInventory) {
			if (itemName.startsWith(item.getName().toLowerCase())) {
				parcel = item;
			}
		}

		if (parcel == null) {
			append(player, "You must specify an item to give!");
			return;
		}

		ArrayList<Ship> ships = player.getPlanet().getAllShips();

		for (Ship ship : ships) {

			if (shipName.equals(ship.getName().toLowerCase())) {

				otherShip = ship;

				if (otherShip instanceof Player) {

					ArrayList<Item> otherInventory = ((Player) otherShip)
							.getInventory();

					otherInventory.add(parcel);
					myInventory.remove(parcel);

					append(player, "Your have given one " + parcel.getName()
							+ " to " + otherShip.getName() + ".");
					append((Player) otherShip,
							"Your transaction with " + player.getName()
									+ " is successful. " + parcel.getName()
									+ " has been added to your inventory.");
					return;

				} else if (otherShip instanceof Merchant) {
					player.sell(parcel);
					return;

				}

			}
		}

		if (otherShip != null) {
			append(player, otherShip.getName() + " won't receive your item.");
			return;
		} else {
			append(player, "You must specify who you want to give the "
					+ parcel.getName() + " to.");
		}
	}

	/**
	 * A player picks up the specified item from the local planet's debris
	 * field, if it is there. If not, a message is sent to the user.
	 * 
	 * @param player
	 * @param itemToAdd
	 */
	private synchronized void get(Player player, String itemToAdd) {

		ArrayList<Item> inventory = player.getInventory();
		ArrayList<Item> debris = player.getPlanet().getDebris();

		boolean found = false;

		Item itemRetrieved = null;
		
		for (int i = 0; i < debris.size(); i++) {
			if (itemToAdd.startsWith(debris.get(i).getName().toLowerCase())) {
				itemRetrieved = debris.get(i);
				inventory.add(itemRetrieved);
				debris.remove(i);
				found = true;
				break;
			}
		}

		if (found) {
			append(player, "You have picked up a " + itemRetrieved.getName() + " from "
					+ player.getPlanet().getName() + "'s debris field.");
		} else {
			append(player, "That item cannot be found in "
					+ player.getPlanet().getName() + "'s debris field.");
		}
	}

	/**
	 * The player drops the specified item (and it is added to the local
	 * planet's debris field).
	 * 
	 * @param player
	 * @param itemToDrop
	 */
	private synchronized void drop(Player player, String itemToDrop) {

		ArrayList<Item> inventory = player.getInventory();
		ArrayList<Item> debris = player.getPlanet().getDebris();

		boolean found = false;

		for (int i = 0; i < inventory.size(); i++) {
			if (itemToDrop.startsWith(inventory.get(i).getName().toLowerCase())) {
				debris.add(inventory.get(i));
				inventory.remove(i);
				found = true;
				break;
			}
		}

		if (found) {
			append(player, "You have dropped " + itemToDrop + ".");
		} else {
			append(player, "You do not have that item in your inventory!");
		}
	}

	/**
	 * Helper method that parses the player's "attack" command. If a valid ship
	 * is found, initiates combat; otherwise, returns an error message.
	 * 
	 * @param player
	 * @param shipName
	 */
	private synchronized void attack(Player player, String shipName) {

		Ship defender = null;

		if (shipName.equalsIgnoreCase(player.getName())) {
			append(player, "We can't do that; that would be suicide!");
			return;
		}

		Planet planet = player.getPlanet();
		ArrayList<Ship> orbiters = planet.getAllShips();

		for (int i = 0; i < orbiters.size(); i++) {
			if (shipName.startsWith(orbiters.get(i).getName().toLowerCase())) {
				CombatHandler ch = new CombatHandler();
				defender = orbiters.get(i);
				ch.attack(player, defender);
				return;
			}
		}

		if (defender == null) {
			append(player, "That ship is not currently orbiting this planet!");
		}
	}

	/**
	 * A helper method that parses the player's command to "engage" a MOB in a
	 * non-hostile way (usually resulting in non-hostile communication). An
	 * attempt to engage another player results in an error; players should use
	 * the chat function if they want to communicate with other players.
	 * 
	 * @param player
	 * @param shipName
	 */
	private synchronized void engage(Player player, String shipName) {

		Ship responder = null;

		if (shipName.startsWith(player.getName().toLowerCase())) {
			append(player, "Talking to yourself again?");
			return;
		}

		ArrayList<Ship> orbiters = player.getPlanet().getAllShips();

		for (int i = 0; i < orbiters.size(); i++) {
			if (shipName.startsWith(orbiters.get(i).getName().toLowerCase())) {
				responder = orbiters.get(i);

				if (responder instanceof Player) {
					append(player,
							"Use the chat function if you want to chat with another player!");
					return;
				} else {
					((Mob) responder).respond(player);
					return;
				}
			}
		}

		if (responder == null) {
			append(player, "That ship is not currently orbiting this planet!");
		}

	}

	/**
	 * A generic "look" command gives the player a description of the local
	 * planet and nearby ships.
	 * 
	 * @param player
	 */
	private synchronized void look(Player player) {

		String output = "";

		// planet description
		Planet planet = player.getPlanet();
		output += "You are orbiting " + planet.getName() + ".\n";
		output += planet.getDescription() + "\n";

		// planet resources
		ArrayList<Resource> resources = planet.getResources();
		if(resources.size() != 0) {
			output += planet.getName() + " has resources that can be mined if you have a drill bit: ";
			for (int i = 0; i < resources.size(); i++) {
				output += " " + resources.get(i).getName();
				if (i == resources.size() - 1) {
					output += ".\n";
				} else {
					output += ", ";
				}
			}
		}
		
		// the "debris" from which a player can "get" items
		ArrayList<Item> debris = player.getPlanet().getDebris();
		output += "A ring of space debris orbiting "
				+ player.getPlanet().getName() + " contains ";
		if (debris.size() == 0) {
			output += "no useful items.\n";
		} else {
			for (int i = 0; i < debris.size(); i++) {
				output += "a " + debris.get(i).getName();
				if (i == debris.size() - 1) {
					output += ".\n";
				} else {
					output += " and ";
				}
			}
		}

		// list of all ships orbiting the planet
		ArrayList<Ship> ships = planet.getAllShips();
		for (Ship ship : ships) {

			if (!ship.getName().equals(player.getName())) {
				output += ship.toString() + "\n";
			}
		}

		output += "Directions you can move: ";

		if (planet.getNeighbor(Direction.NORTH) != null) {
			output += "North, ";
		}
		if (planet.getNeighbor(Direction.SOUTH) != null) {
			output += "South, ";
		}
		if (planet.getNeighbor(Direction.EAST) != null) {
			output += "East, ";
		}
		if (planet.getNeighbor(Direction.WEST) != null) {
			output += "West, ";
		}
		output = output.substring(0, output.length() - 2) + ".";

		append(player, output);
	}

	/**
	 * A "look" command followed by a second word can generate a textual
	 * representation of the player's inventory or equipped items, or the debris
	 * field orbiting the planet (i.e. where the "dropped" items are stored).
	 * NB: this incorporates the "inventory" command.
	 * 
	 * @param player
	 * @param command
	 */
	private synchronized void lookAt(Player player, String toSee) {

		Item toExamine = null;

		// items in "room"
		ArrayList<Item> debris = player.getPlanet().getDebris();
		for (Item item : debris) {
			if (toSee.startsWith(item.getName().toLowerCase())) {
				toExamine = item;
				append(player, toExamine.getDescription());
				return;
			}
		}

		// items in inventory
		ArrayList<Item> inventory = player.getInventory();
		for (Item item : inventory) {
			if (toSee.startsWith(item.getName().toLowerCase())) {
				toExamine = item;
				append(player, toExamine.getDescription());
				return;
			}
		}

		// items equipped
		ArrayList<Item> equipped = player.getEquipped();
		for (Item item : equipped) {
			if (toSee.startsWith(item.getName().toLowerCase())) {
				toExamine = item;
				append(player, toExamine.getDescription());
				return;
			}
		}

		Ship shipToSee = null;

		// other npcs/players in "room"
		ArrayList<Ship> orbiters = player.getPlanet().getAllShips();
		for (Ship ship : orbiters) {
			if (toSee.startsWith(ship.getName().toLowerCase())) {
				shipToSee = ship;
				append(player, shipToSee.getDescription());
				return;
			}
		}

		append(player, "The item or ship you specified was not found!");
	}

	/**
	 * Player is added to game & recognized by controller.
	 * 
	 * @param player
	 */
	public synchronized void addPlayer(Player player) {

		Random random = new Random();
		int planetIndex = random.nextInt(37);
		Planet planet = galaxy.getPlanetByIndex(planetIndex);
		player.setPlanet(planet);

	}

	/**
	 * Returns the controller's galaxy.
	 * 
	 * @return
	 */
	public synchronized Galaxy getGalaxy() {
		return galaxy;
	}

	/**
	 * Computes combat between two ships, and appends compiled combat report
	 * from CombatHandler object.
	 * 
	 * @param attacker
	 * @param defender
	 */
	public synchronized void processCombat(Ship attacker, Ship defender) {

		CombatHandler ch = new CombatHandler();

		ch.attack(attacker, defender);
	}

	/**
	 * Used to append text to the game GUI
	 * 
	 * @param message
	 */
	public synchronized void append(Player player, String text) {

		Liason liason = liasonsByPlayer.get(player);

		if (liason != null) {
			liason.sendMessageToClient(text);
		} else {
			System.out.println("Warning: No client to send message to.");
		}
	}

	/**
	 * @return ClientList get the player base
	 */
	public ClientList getPlayerBase() {
		return playerBase;
	}

	/**
	 * Method insures that file can be found regardless of the OS that this
	 * program is being ran on
	 */
	public static String baseDir = System.getProperty("user.dir")
			+ File.separator + "TextDocs" + File.separator;

	/**
	 * Reads and appends the file for commands to the text area
	 */
	public void getCommands(Player me) {
		String commands = "";
		Scanner s = null;
		try {
			s = new Scanner(new File(baseDir + "commands.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		while (s.hasNextLine()) {
			commands += s.nextLine() + "\n";
		}
		s.close();
		append(me, commands);
	}

}
