package ships;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import Galaxy.Planet;
import Items.Hull;
import Items.Shield;
import Items.Weapon;
import Model.Controller;

/**
 * The Philanthropist mob donates small amounts of currency to poor players.
 * This is only on the philanthropist's whim; the player engaging the
 * philanthropist yields no more money.
 * 
 * @author Connor James Lewis
 * 
 */
public class Philanthropist extends ActingMob implements Serializable {

	public Philanthropist(String title, int[] route) {

		super(title, route, 75000);

		this.setAwareness(150);

		this.setStats(new Hull(MAX_ITEM_LEVEL), new Weapon(1), new Shield(
				MAX_ITEM_LEVEL));
	}

	/**
	 * The Philanthropist gives a small amount of money to players who have very
	 * little currency.
	 */
	@Override
	protected void perform(Player player) {

		if (player.getWallet() < 20) {

			String donorMessage = this.name
					+ " has taken pity on you and decided to ameliorate your "
					+ "poverty with a gift of 5 credits.";
			player.addCurrency(5);
			Controller.getInstance().append(player, donorMessage);
		}
	}

	/**
	 * This ship only initiates donation; it will not respond to the player
	 * attempt to engage.
	 */
	@Override
	public void respond(Player player) {

		String response = this.name
				+ "'s comms channels are closed for a private charity dinner.";
		Controller.getInstance().append(player, response);

	}

	/**
	 * 
	 */
	@Override
	public String toString() {

		return this.name + " blares classical music into the void.";
	}
	
	/**
	 * Returns a description of the philanthropist and its behavior.
	 */
	@Override
	public String getDescription() {

		return name
				+ " belongs to a wealthy philanthropist who might throw some "
				+ "money your way if you're very low on credits, but otherwise "
				+ "will have nothing to do with you. He is well-armored but not "
				+ "very powerful in combat.";
	}
}
