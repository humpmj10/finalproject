	Planet Venom: some of you may have heard its name. Once home
to a mad scientist, banished to this desolate waste, this place is 
crawling with lost technology and destructive pirates... and worse.
There is an active government here, run from the Walhart Imperial Forces.
Landing and mining rights are expensive, and the police over react,
as in all planets under Walhart control. 
	Despite their influence here, relics from the mad scientist still exist,
and pirates and aliens alike run rampant and are exceptionally powerful.
On the surface, you will not likely find shops, but treasure abounds,
and undiscovered technologies are everywhere. And more dangers lurk still.
Venom is for the hardiest of adventurers, ready to take on the challenge
of beauracracy, unruly outlaws, an unhospitable wasteland, and what remains
of an ancient madman, all for the sake of some of the greatest treasures.	
	