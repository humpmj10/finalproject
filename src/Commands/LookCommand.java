package Commands;

import ships.Player;

/**
 * Executes a look command.
 * 
 * @author Michael Humphrey
 * 
 */
public class LookCommand extends TransactionCommand {

	private Player player;

	/**
	 * Set the player for this command
	 * 
	 * @param player
	 *            Player that will be looking.
	 */
	public LookCommand(Player player) {
		this.player = player;
	}

	public void execute() {
		// TODO Auto-generated method stub
		System.out.println("I looked"); // will need to implemented
	}

}
