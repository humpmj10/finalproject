package ships;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;

import javax.swing.Timer;

import Galaxy.*;
import Items.*;

/**
 * This superclass defines the basic interactions and properties of mobile NPCs.
 * 
 * @author Connor Lewis
 */
public abstract class Mob extends Ship implements Serializable {

	// each MOB travels its own trajectory at a given speed (type-dependent)
	private ArrayList<Planet> path;
	//private transient ListIterator<Planet> routeIterator;
	private int routeIndex;
	private int timeInEachSystem;
	// equipped upgrades, which are used to calculate stats
	protected Item loot;

	protected final int MAX_ITEM_LEVEL = 5;

	/**
	 * Constructor for the Mob object.
	 */
	protected Mob(String title, int[] route, int timeInSystem) {
		super(title);

		Galaxy gal = Galaxy.getInstance();

		path = gal.getPlanetsFromIndices(route);
		timeInEachSystem = timeInSystem;
		
		this.currentPlanet = path.get(0);
		routeIndex = 0;

		Item[] selection = {
				new Hull(1),
				new Hull(1),
				new Hull(1),
//				new Shield(1),
//				new Shield(1),
				new Weapon(1),
				new Weapon(1),
				new Fuel(),
				new Fuel(),
				new Fuel(),
				new Hull(2),
				new Shield(2),
				new Artefact("Strange Orb",
						"A strange alien orb that hums unnervingly.") };
		setLoot(selection);
		
		setPath();
	}

	/**
	 * Each Mob carries with it a random piece of low-level loot which the
	 * player has the opportunity to acquire if the player defeats the Mob in
	 * combat. However, some Mobs have specific and/or more valuable loot.
	 */
	protected void setLoot(Item[] selection) {

		Random random = new Random();
		int choose = random.nextInt(selection.length);

		loot = selection[choose];
	}

	/**
	 * Returns this Mob's loot; in the event the mob loses a battle, its loot is
	 * awarded to the victor (it does not disappear from the mob's possession,
	 * however).
	 * 
	 * @return
	 */
	public synchronized Item getLoot() {

		return loot;
	}

	/**
	 * Used to construct the mobility for a given MOB. The MOB follows a given
	 * route, passed in as a list of integers numbering the planets in its path.
	 * For most MOBs, the list of planets must be adjacent and the MOB must move
	 * either in a circle or an arc. It stops at each planet for a given period
	 * of time before moving to the next on its route.
	 * 
	 * @param path
	 *            an ArrayList of planets along its path
	 * @param timeInEachSystem
	 *            the time (in ms) it spends orbiting each planet along its path
	 */
	public void setPath() {

		Timer timer = new Timer(timeInEachSystem, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {

				if (routeIndex >= path.size()) {
					routeIndex = 0;
				}

				move(path.get(routeIndex));
				routeIndex++;
			}

		});
		
		timer.start();
	}

	/**
	 * The player can provoke a response from any Mob through friendly
	 * interaction. This can initiate trading and conversations with some mobs
	 * and prompt attack from others, depending on mob type.
	 */
	public abstract void respond(Player player);

	/**
	 * 
	 */
	@Override
	public String toString() {

		return "The ";
	}
}