	This sector of space contains no planet, but is far from empty. It is a hazardous
asteroid field, riddled with pirates far from nearby planet Tyrion's reach;
stray vessels oft find themselves robbed blind with no government to defend them,
or are destroyed by the asteroids. Many an abandoned ship lies here still,
and some parts still unclaimed. The Ship's Graveyard is well named.
	