package Commands;

/**
 * Command that lists all the items in the player's inventory.
 * 
 * @author Michael Humphrey
 * 
 */
public class ListCommand extends TransactionCommand {

	private CommandReceiver cr;

	public ListCommand() {
		this.cr = cr.getHandle();
	}

	public void execute() {
		cr.methListItems();
	}

}
