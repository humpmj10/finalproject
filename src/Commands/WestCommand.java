package Commands;

import ships.Player;

/**
 * Class that holds the execute method to move the player north once a request
 * has been recieved by the client.
 * 
 * @author Michael Humphrey
 * 
 */
public class WestCommand extends TransactionCommand {

	private CommandReceiver cr;

	/**
	 * Set the player for this command
	 * 
	 * @param player
	 *            Player that will be moving.
	 */
	public WestCommand() {
		this.cr = cr.getHandle();
	}

	/**
	 * This class will move the player when execute has been called.
	 */
	@Override
	public void execute() {
		cr.methMoveWest();
	}

}