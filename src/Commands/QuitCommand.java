package Commands;

/**
 * Command when player wishes to exit the program and disconnect from the
 * server.
 * 
 * @author Michael Humphrey
 * 
 */
public class QuitCommand extends TransactionCommand {

	private CommandReceiver cr;

	public QuitCommand() {
		this.cr = cr.getHandle();
	}

	public void execute() {
		cr.methPlayerQuit();
	}
}
