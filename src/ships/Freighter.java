package ships;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import Galaxy.Planet;
import Items.*;

/**
 * The Freighter is a passive Mob. It has relatively low stats and carries
 * high-level resources that can be acquired through attacking it; however, this
 * is viewed as a hostile action by the Police/Governments. The Freighter serves
 * no other purpose.
 * 
 * @author Connor Lewis
 * 
 */
public class Freighter extends Mob implements Serializable {

	public Freighter(String title, int[] route) {
		// the Freighter's stopover at each planet is randomly calculated to be
		// between 120 and 90 seconds (not final)
		super(title, route, 120000 - (new Random()).nextInt(15001));

		// freighters carry high quality resources
		Item[] selection = { new Minerals(4), new Minerals(5), new Metals(4),
				new Metals(5) };
		this.setLoot(selection);

		this.setStats(new Hull(3), new Weapon(1), new Shield(3));
	}

	/**
	 * Only response to player is generic greeting.
	 */
	@Override
	public void respond(Player player) {

		// append to GUI: name + " responds with a bored hail.";

	}

	/**
	 * 
	 */
	@Override
	public String toString() {

		return "The cargo freighter " + name
				+ " trundles slowly through space.";
	}

	@Override
	public String getDescription() {

		return name
				+ " is a cargo transport. This sort of ship has little interaction "
				+ "with other ships, its only purposes being to ferry expensive "
				+ "resources from planet to planet.";
	}

}