package ships;

import java.io.Serializable;

import Items.*;
import Model.Controller;

/**
 * The Police object is a type of MOB that is by default neutral toward the
 * player. If the player attacks a non-hostile NPC (e.g. government, merchant,
 * etc), the Police becomes hostile for a period of time and will attack the
 * player. Unlike most MOBs, its behavior and stats are uniform/hard-coded.
 * 
 * @author Connor Lewis
 * 
 */
public class Police extends Mob implements Serializable {

	public Police(String title, int[] route) {
		super(title, route, 60000);

		this.setStats(new Hull(MAX_ITEM_LEVEL - 1), new Weapon(
				MAX_ITEM_LEVEL - 1), new Shield(MAX_ITEM_LEVEL - 1));
		
		this.setLoot(new Item[] { new ShieldSpecialist()} );
	}

	@Override
	public synchronized void respond(Player player) {
		String message = "The police cruiser "
				+ this.name
				+ " has responded: \"Behave yourself, citizen, and we'll have nothing to talk about.\"";
		Controller.getInstance().append(player, message);
	}

	/**
	 * A method that determines the Police ship's unique type of initiated
	 * interaction. It is called every time there is combat at the same planet
	 * it is currently orbiting, and results in an attack upon the player.
	 * 
	 * @param attacker
	 * @param defender
	 */
	public synchronized void crimeWatch(Player attacker, Ship defender) {

		if (!(defender instanceof Pirate) && !(defender instanceof Alien)
				&& !(defender instanceof Thief)) {

			String message = "You have broken galactic law by attacking "
					+ defender.name + " and will be prosecuted for your crime!";
			Controller.getInstance().append(attacker, message);
			Controller.getInstance().processCombat(this, attacker);
		}
	}

	/**
	 * 
	 */
	@Override
	public String toString() {

		return "The police cruiser " + name + " patrols the "
				+ this.currentPlanet.getName() + " skies.";
	}

	/**
	 * Returns a description of the police ship.
	 */
	@Override
	public String getDescription() {

		return name
				+ " is a police cruiser that will leave you alone unless you attack "
				+ "a friendly ship. Friendly ships include merchants, scientist ships, "
				+ "cargo ships, and engineers. If you do attack one of these, "
				+ name
				+ " will attack you for breaking the law. Police are powerful "
				+ "and should be avoided.";
	}
}