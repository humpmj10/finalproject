	Lusitania: A site of a fierce battle of Walhart's forces against an 
alliance of opposing systems. An empty sector of space and currently 
no-man's land. Named after the largest dreadnaught (ship) ever built,
the Lusitania was destroyed here by Walhart, igniting the fierce conflict;
ending in stalemate. With nowhere to land, you won't find much action,
but for wandering pirates and lost soldiers flying by.