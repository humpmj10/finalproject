package Commands;

/**
 * Will execute the create an account command. Needs arguments for
 * password/username.
 * 
 * @author Michael Humphrey
 * 
 */
public class CreateaccountCommand extends TransactionCommand {

	private CommandReceiver cr;

	/**
	 * Arguments are stored as String, String in an array with the
	 * username/password respectively.
	 * 
	 * @param argument
	 *            Object[] that stores the username/password
	 */
	public CreateaccountCommand() {
		this.cr = cr.getHandle();
	}

	public void execute() {
		cr.methCreateAccount();
	}
}
