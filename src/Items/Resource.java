package Items;

import java.io.Serializable;

/**
 * This subclass of Item functions as the superclass for types of resource such
 * as minerals, metals etc. Resources have "tiers" that are automatically
 * calculated when constructed; e.g. "new Mineral(1)" will yield the cheapest
 * type of mineral. As with upgrades, the value and name of that specific
 * leveled resource are calculated based on the tier provided.
 * 
 */
public abstract class Resource extends Item {

	protected int itemTier;

	public Resource(int itemTier) {
		super();

		if (itemTier <= 0)
			this.itemTier = 1;
		else if (itemTier > 5)
			this.itemTier = 5;
		else
			this.itemTier = itemTier;

	}

	public int getTier() {
		return this.itemTier;
	}
}