package Tests;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Vector;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Commands.CommandArgument;
import Commands.CommandManager;
import Commands.LookCommand;
import Commands.SayCommand;
import Commands.TransactionCommand;

/**
 * Test class for commands. Test each one independently except for move which
 * tests all four in a row to make sure the Receiver can handle multiple
 * requests in a row and execute them in order.
 * 
 * @author Michael Humphrey
 * 
 */
public class CommandTest {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

	@Before
	public void setUpStreams() {
		System.setOut(new PrintStream(outContent));
		System.setErr(new PrintStream(errContent));
	}

	@After
	public void cleanUpStreams() {
		System.setOut(null);
		System.setErr(null);
	}

	/**
	 * Test Move player. Right now is only a string message that the player has
	 * moved in whatever direction.
	 */
	@Test
	public void testMovePlayerInAllFourDirections() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		commandList.add("North");
		commandList.add("South");
		commandList.add("East");
		commandList.add("West");
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals("Hey I went North." + "\n" + "Hey I went South." + "\n"
				+ "Hey I went East." + "\n" + "Hey I went West.", outContent
				.toString().trim());
	}

	/**
	 * Tests the look command.
	 */
	@Test
	public void testLookCommand() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		commandList.add("Look");
		CommandArgument commandArg = new CommandArgument();
		String username = "Bob";
		commandArg.setArgument(username, "");
		argList.add(commandArg);
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals("Hey I looked.", outContent.toString().trim());
	}

	/**
	 * Test the say command.
	 */
	@Test
	public void testSayCommand() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		CommandArgument commandArg = new CommandArgument();
		String message = "Hello People in my room";
		String username = "bob";
		commandArg.setArgument(username, message);
		argList.add(commandArg);
		commandList.add("Say");
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals(
				"My username is: bob My message is: Hello People in my room",
				outContent.toString().trim());

	}

	/**
	 * Test the OOC command.
	 */
	@Test
	public void testOOCCommand() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		CommandArgument commandArg = new CommandArgument();
		String message = "Hello Everyone";
		String username = "bob";
		commandArg.setArgument(username, message);
		argList.add(commandArg);
		commandList.add("OOC");
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals("My username is: bob My message is: Hello Everyone",
				outContent.toString().trim());

	}

	/**
	 * Test the OOC command.
	 */
	@Test
	public void testTellCommand() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		CommandArgument commandArg = new CommandArgument();
		String message = "Hello you";
		String username = "bob";
		commandArg.setArgument(username, message);
		argList.add(commandArg);
		commandList.add("Tell");
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals("My username is: bob My message is: Hello you", outContent
				.toString().trim());

	}

	/**
	 * Test createAccount, right now is only a string message that the account
	 * was created.
	 */
	@Test
	public void testCreateAccount() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		CommandArgument commandArg = new CommandArgument();
		commandArg.setArgument("bob", "1234");
		argList.add(commandArg);
		commandList.add("CreateAccount");
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals(
				"You have created an account username: bob password: 1234",
				outContent.toString().trim());
	}

	/**
	 * Test player quit command.
	 */
	@Test
	public void testPlayerQuit() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		CommandArgument commandArg = new CommandArgument();
		commandList.add("Quit");
		commandArg.setArgument(null, null); // null right now, not sure how this will work
		argList.add(commandArg);
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals("Player has quit.", outContent.toString().trim());
	}

	/**
	 * Test getInventory
	 */
	@Test
	public void testGetInventory() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		CommandArgument commandArg = new CommandArgument();
		commandList.add("Inventory");
		commandArg.setArgument(null, null); // null right now, not sure how this will work
		argList.add(commandArg);
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals("This is what you got.", outContent.toString().trim());
	}

	/**
	 * Test getInventory
	 */
	@Test
	public void testUse() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		CommandArgument commandArg = new CommandArgument();
		commandList.add("Use");
		String username = "bob";
		String item = "shield";
		commandArg.setArgument(username, item); // not fully implemented, just a placeholder for now
		argList.add(commandArg);
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals("My username is: bob The item I used was: shield",
				outContent.toString().trim());
	}

	/**
	 * Test dropItem
	 */
	@Test
	public void testDropItem() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		CommandArgument commandArg = new CommandArgument();
		commandList.add("Drop");
		String username = "bob";
		String item = "orb";
		commandArg.setArgument(username, item); // not fully implemented, just a placeholder for now
		argList.add(commandArg);
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals("My username is: bob The item I dropped was: orb",
				outContent.toString().trim());
	}

	/**
	 * Test getItem from a specific room
	 */
	@Test
	public void testGetItem() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		CommandArgument commandArg = new CommandArgument();
		commandList.add("Get");
		String username = "bob";
		String item = "orb";
		commandArg.setArgument(username, item); // not fully implemented, just a placeholder for now
		argList.add(commandArg);
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals("My username is: bob The item I dropped was: orb",
				outContent.toString().trim());
	}

	/**
	 * Test getItem from a specific room
	 */
	@Test
	public void testListItems() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		CommandArgument commandArg = new CommandArgument();
		commandList.add("List");
		ArrayList<String> items = new ArrayList<String>();
		items.add("shield");
		items.add("sword");
		items.add("axe");
		commandArg.setArgument(items); // not fully implemented, just a placeholder for now
		argList.add(commandArg);
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals("[shield, sword, axe]", outContent.toString().trim());
	}

	/**
	 * Test Who command
	 */
	@Test
	public void testWho() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		CommandArgument commandArg = new CommandArgument();
		commandList.add("Who");
		ArrayList<String> players = new ArrayList<String>();
		players.add("Bobby");
		players.add("Mike");
		players.add("Lilly");
		commandArg.setArgument(players); // not fully implemented, just a placeholder for now
		argList.add(commandArg);
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals("[Bobby, Mike, Lilly]", outContent.toString().trim());
	}

	/**
	 * Test Score command
	 */
	@Test
	public void testScore() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		CommandArgument commandArg = new CommandArgument();
		commandList.add("Score");
		ArrayList<String> stats = new ArrayList<String>();
		stats.add("Health");
		stats.add("Shields");
		stats.add("Race");
		commandArg.setArgument(stats); // not fully implemented, just a placeholder for now
		argList.add(commandArg);
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals("[Health, Shields, Race]", outContent.toString().trim());
	}

	/**
	 * Test Commands command, lists all commands that are available.
	 */
	@Test
	public void testCommands() {
		Vector<String> commandList = new Vector<String>();
		Vector<CommandArgument> argList = new Vector<CommandArgument>();
		CommandArgument commandArg = new CommandArgument();
		commandList.add("Commands");
		ArrayList<String> commands = new ArrayList<String>();
		commands.add("score");
		commands.add("commands");
		commands.add("who");
		commandArg.setArgument(commands); // not fully implemented, just a placeholder for now
		argList.add(commandArg);
		TransactionCommand tc = new TransactionCommand(commandList, argList);
		CommandManager cm = new CommandManager(tc);
		cm.runCommands();
		assertEquals("[score, commands, who]", outContent.toString().trim());
	}
}
