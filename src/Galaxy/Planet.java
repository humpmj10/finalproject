package Galaxy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import Items.Item;
import Items.Resource;
import Model.Controller;
import ships.*;

/**
 * 
 * @author Brent Murdock Class defines planets, uses Strategy design for
 *         governments within planet.
 */
public class Planet implements Serializable {

	private final String name;
	private ArrayList<Resource> resources;
	// an list of all players and mobs currently orbiting this planet
	private ArrayList<Ship> orbiters;
	private ArrayList<Item> debris;
	private HashMap<Direction, Planet> neighbors;

	public Planet(String name) {
		this.name = name;
		this.resources = new ArrayList<Resource>(); // For now.
		this.orbiters = new ArrayList<Ship>();
		this.debris = new ArrayList<Item>();
		this.neighbors = new HashMap<Direction, Planet>();

	}

	/**
	 * Assigns the planet its neighbors. Used in constructing Galaxy.
	 * 
	 * @param direction
	 * @param planet
	 */
	public void setNeighbor(Direction direction, Planet planet) {
		neighbors.put(direction, planet);
	}

	/**
	 * Returns the Planet in a given direction from this planet.
	 * 
	 * @param direction
	 * @return
	 */
	public Planet getNeighbor(Direction direction) {
		return neighbors.get(direction);
	}

	/**
	 * 
	 * @return String Returns the planet's name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @return Tentative: may return list of resources.
	 */
	public ArrayList<Resource> getResources() {
		return resources;
	}

	/**
	 * Method insures that file can be found regardless of the OS that this
	 * program is being ran on
	 */
	public static String baseDir = System.getProperty("user.dir")
			+ File.separator + "TextDocs" + File.separator;

	/**
	 * 
	 * @return String Gives the user a description of the planet. Uses a file
	 *         given by the planet's name. Only works if the file exists, of
	 *         course.
	 */
	public String getDescription() {
		String desc = name + ": \n";
		Scanner s = null;
		try {
			s = new Scanner(new File(baseDir + name + ".txt"));
			while (s.hasNextLine()) {
				desc += s.nextLine() + "\n";
			}
			s.close();
		} catch (FileNotFoundException e) {

			// e.printStackTrace();
			System.out.println("File probably doesn't exist yet because lazy.");
		}

		return desc.trim();
	}

	/**
	 * Removes the ship from the list of orbiting ships. Called when ship leaves
	 * orbit.
	 * 
	 * @param ship
	 */
	public synchronized void removeOrbiter(Ship ship) {

		orbiters.remove(ship);

		Ship inOrbit = null;
		for (int i = 0; i < orbiters.size(); i++) {
			inOrbit = orbiters.get(i);
			if (inOrbit instanceof Player) {
				Controller c = Controller.getInstance();
				String entryMessage = ship.getName() + " has left "
						+ this.getName() + "'s orbit.";
				c.append((Player) inOrbit, entryMessage);
			}
		}
	}

	/**
	 * Adds a ship to the current list of orbiting ships. Called when the ship
	 * enters orbit.
	 * 
	 * @param ship
	 */
	public synchronized void addOrbiter(Ship ship) {

		orbiters.add(ship);
		ship.setPlanet(this);

		if (ship instanceof Player) {
			Controller.getInstance().readCommand((Player) ship, "look");
		}

		Ship inOrbit = null;
		if (ship instanceof Player) {
			for (int i = 0; i < orbiters.size(); i++) {
				inOrbit = orbiters.get(i);
				if (inOrbit instanceof ActingMob) {
					((ActingMob) inOrbit).act((Player) ship);
				}
			}
		}

		inOrbit = null;
		for (int i = 0; i < orbiters.size(); i++) {
			inOrbit = orbiters.get(i);
			if (inOrbit instanceof Player) {
				Controller c = Controller.getInstance();
				String entryMessage = ship.getName() + " has entered "
						+ this.getName() + "'s orbit.";
				if (!ship.getName().equals(inOrbit.getName())) {
					c.append((Player) inOrbit, entryMessage);
				}
				if (ship instanceof ActingMob) {
					((ActingMob) ship).act((Player) inOrbit);
				}
			}
		}

	}

	/**
	 * Function like addOrbiter() but called when a player is initialized; that
	 * way, all the local mobs are not immediately alerted to the player's
	 * presence.
	 * 
	 * @param player
	 */
	public synchronized void dropPlayerIntoOrbit(Player player) {

		orbiters.add(player);

	}

	/**
	 * Returns all the ships of a given type currently orbiting this planet.
	 * 
	 * @param ship
	 * @return
	 */
	public synchronized ArrayList<Ship> getShipsOfType(Class ship) {

		ArrayList<Ship> selectedOrbiters = new ArrayList<Ship>();

		for (int i = 0; i < orbiters.size(); i++) {
			Ship inOrbit = orbiters.get(i);
			if (ship.isInstance(inOrbit)) {
				selectedOrbiters.add(inOrbit);
			}
		}

		return selectedOrbiters;
	}

	/**
	 * Returns an arraylist containing the list all Mobs and players currently
	 * orbiting this planet.
	 * 
	 * @return
	 */
	public synchronized ArrayList<Ship> getAllShips() {
		return orbiters;
	}

	/**
	 * This is where items are stored once they are "dropped" by ships orbiting
	 * the planet.
	 * 
	 * @return
	 */
	public synchronized ArrayList<Item> getDebris() {

		return debris;
	}

	/**
	 * Adds an item to the debris field.
	 * 
	 * @param item
	 */
	public synchronized void addDebris(Item item) {

		debris.add(item);
	}

	/**
	 * Removes an item from the planet's debris field.
	 * 
	 * @param item
	 */
	public synchronized void removeDebris(Item item) {

		debris.remove(item);
	}

	/**
	 * Adds a resource to a planet.
	 * 
	 * @param resource
	 */
	public synchronized void addResource(Resource resource) {
		resources.add(resource);
	}

	/**
	 * Returns an arraylist of all the planet's resources.
	 * 
	 * @param resource
	 * @return
	 */
	public ArrayList<Resource> getResources(Resource resource) {
		return resources;
	}
}