package Commands;

/**
 * Command used when the player wishes to speak to someone directly.
 * 
 * @author Michael Humphrey
 * 
 */
public class TellCommand extends TransactionCommand {

	private CommandReceiver cr;

	public TellCommand() {
		this.cr = cr.getHandle();
	}

	public void execute() {
		cr.methTell();
	}
}
