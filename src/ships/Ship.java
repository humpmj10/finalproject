package ships;

import java.io.Serializable;
import java.util.HashMap;

import Galaxy.*;
import Items.Hull;
import Items.Shield;
import Items.Weapon;
import Model.Controller;


public abstract class Ship implements Serializable {

	// each player and mob has its own title; e.g a Pirate named
	// "Dread Pirate Roberts"
	protected String name;
	// health points, attack, defense
	protected HashMap<String, Double> stats;
	protected Planet currentPlanet;

	/**
	 * The constructor, which merely takes the name of the ship as an argument.
	 * Ships are either Players or some type of mobile NPC, called mobs
	 * 
	 * @param title
	 *            the name of the ship
	 */
	public Ship(String title) {
		this.name = title;

		stats = new HashMap<String, Double>();
		stats.put("health", 50.0);
		stats.put("attack", 0.0);
		stats.put("defense", 0.0);

	}

	/**
	 * Assign the player's current planet
	 * 
	 * @param planet
	 */
	public synchronized void setPlanet(Planet planet) {
		currentPlanet = planet;
	}
	
	/**
	 * Moves the ship from one planet to the next. From the Planet object's
	 * side, this ship is removed from the currentPlanet's list of orbiters and
	 * added to next's list of orbiters.
	 * 
	 * @param next
	 *            the next planet in this ship's trajectory
	 * @return true if the ship has moved to the next planet; false otherwise
	 */
	protected synchronized boolean move(Planet next) {

		if (Controller.getInstance().move(this, next, currentPlanet)) {
			currentPlanet = next;
			return true;
		} else {
			return false;
		}

	}

	/**
	 * A helper method used to construct the statistics of the ship. It is
	 * assigned health points, attack and defense, based on upgrades whose tiers
	 * determine its effectiveness at combat with players and MOBs.
	 * 
	 * @param hull
	 *            the tiered Hull upgrade equipped, which affects health
	 * @param weapon
	 *            the tiered Weapon upgrade equipped, which affects attack
	 * @param shield
	 *            the tiered Shield upgrade equipped, which affects defense
	 */
	protected synchronized void setStats(Hull hull, Weapon weapon, Shield shield) {

		stats.put("health", 50.0 + 10.0 * hull.getUpgradeTier());
		stats.put("attack", 0.0 + 10.0 * weapon.getUpgradeTier());
		stats.put("defense", 0.0 + 10.0 * shield.getUpgradeTier());
	}

	/**
	 * Returns the entire list of stats, useful for comparing a specific stat.
	 * 
	 * @return stats The list of stats for the ship.
	 */
	public HashMap<String, Double> getStats() {
		return stats;
	}

	/**
	 * Returns the planet this ship is currently orbiting.
	 * 
	 * @return the planet being orbited
	 */
	public Planet getPlanet() {

		return currentPlanet;
	}

	/**
	 * Returns the ship's name. In the case of the player ship, this equals
	 * their username.
	 * 
	 * @return the ship's name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns a description specific to each type of ship, to be returned when
	 * the "look at" command is entered.
	 * 
	 * @return the ship's description.
	 */
	public abstract String getDescription();
}
