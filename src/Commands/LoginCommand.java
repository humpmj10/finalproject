package Commands;

public class LoginCommand extends TransactionCommand {

	private CommandReceiver cr;

	/**
	 * Executes a Login Command
	 * 
	 * @author Michael Humphrey
	 * 
	 */
	public LoginCommand() {
		this.cr = cr.getHandle();
	}

	public void execute() {
		cr.methLogin();
	}
}
