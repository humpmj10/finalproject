package ships;

import java.io.Serializable;

import Items.*;
import Model.Controller;

/**
 * The Banker moves from Planet to planet and does not initiate interaction with
 * the Player. It is accompanied by a Police mob and if successfully attacked
 * will yield monetary loot instead of an item.
 * 
 * @author Connor Lewis
 * 
 */
public class Banker extends Mob implements Serializable {

	public Banker(String title, int[] route) {
		super(title, route, 60000);

		this.setStats(new Hull(2), new Weapon(1), new Shield(3));
	}

	@Override
	public synchronized void respond(Player player) {
		String message = "The mobile bank " + this.name
				+ " only responds to valued customers. You don't qualify.";
		Controller.getInstance().append(player, message);
	}

	/**
	 * 
	 */
	@Override
	public String toString() {

		return "The " + this.name + " plods through space.";
	}

	/**
	 * Returns a description of the banker ship.
	 */
	@Override
	public String getDescription() {

		return name + " is a mobile banker, an easy target for those who "
				+ "want a quick buck. However, it is accompanied by high-level security.";
	}
}