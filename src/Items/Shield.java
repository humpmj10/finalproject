package Items;

import java.io.Serializable;

/**
 * The Shield upgrade increases a ship's defense stat.
 * 
 * @author Connor James Lewis
 * 
 */
public class Shield extends TieredUpgrade implements Serializable {

	public Shield(int itemTier) {
		super(itemTier);

		// check to make sure assigned tier is inside 1 - 5 range
		if (itemTier > 5) {
			this.tier = 5;
		} else if (itemTier < 1) {
			this.tier = 1;
		} else {
			this.tier = itemTier;
		}

		this.setName();
		this.setValue();
	}

	/**
	 * Automatically sets the Shield's name upon being given the tier.
	 */
	@Override
	protected void setName() {
		String[] tiers = { "Weak Protective Shield",
				"Standard Force Field Generator", "Repulsive Shield",
				"Kinetic Barrier", "Kinetic Armor" };
		this.name = tiers[this.tier - 1];
	}

	/**
	 * Automatically sets the Shield's value upon being given the tier.
	 */
	@Override
	protected void setValue() {
		int[] tiers = { 10, 20, 25, 35, 45 };
		this.value = tiers[this.tier - 1];
	}

	/**
	 * Automatically sets the Shield's description upon being given the tier.
	 */
	@Override
	public String getDescription() {
		String[] tiers = {
				"The Weak Protective Shield is a shield that barely protects a ship.",
				"The Standard Force Field Generator is a mediocre shield that deflects some of other ships' attacks.",
				"The Repulsive Shield is a mid-range shield that provides decent protection from attacks.",
				"The Kinetic Barrier is a strong and expensive shield that deflects most attacks.",
				"The Kinetic Armor is the most powerful and expensive shield available, almost guaranteeing full protection from enemy attacks." };
		return tiers[tier - 1] + " This shield is worth " + this.getValue() + " credits.";
	}

}