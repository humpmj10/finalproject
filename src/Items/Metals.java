package Items;

import java.io.Serializable;
import java.util.ArrayList;

public class Metals extends Resource implements Serializable {

	public Metals(int itemTier) {
		super(itemTier);
		setName();
		setValue();
	}

	@Override
	protected void setName() {

		ArrayList<String> metalNames = new ArrayList<String>();
		metalNames.add("Tin");
		metalNames.add("Aluminum");
		metalNames.add("Chromium");
		metalNames.add("Iron");
		metalNames.add("Steel");
		name = metalNames.get(itemTier - 1);
	}

	@Override
	protected void setValue() {
	
		int[] valueByTier = { 5, 15, 20, 30, 40 };
		
		this.value = valueByTier[itemTier - 1];

	}

	@Override
	public String getDescription() {

		String result = this.name + " is ";

		if (name.equals("Tin"))
			result += "a soft faintly  metallic element that is malleable and ductile at ordinary\n"
					+ "temperatures and that is used especially in containers, as a protective coating,\n"
					+ " in tinfoil, and in soft solders and alloys\n";
		if (name.equals("Aluminum"))
			result += "a bluish silver-white malleable ductile light trivalent metallic element that has good electrical and thermal conductivity, high reflectivity, and resistance to oxidation and is the most abundant metal"
					+ "\n in the planet's crust where it always occurs in combination\n";
		if (name.equals("Iron"))
			result += "a silver-white malleable ductile magnetic heavy metallic element that readily rusts in moist air, occurs native in meteorites and combined in most igneous rocks,\n"
					+ "is the most used of metals, and is vital to biological processes\n ";
		if (name.equals("Steel"))
			result += "a commercial iron that contains carbon in a"
					+ "ny amount up to about 1.7 percent as an essential alloying constituent, is malleable when under suitable conditions, and is distinguished +\n"
					+ "from cast iron by its malleability and lower carbon content\n";
		else
			result += "a blue-white metallic element found naturally only in combination\n"
					+ "and used especially in alloys and in\n";
		return result + " This metal is worth " + this.value + " credits.";
	}
}
