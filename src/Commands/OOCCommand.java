package Commands;

/**
 * Command when player wishes to speak to everyone in the MUD server.
 * 
 * @author Michael Humphrey
 * 
 */
public class OOCCommand extends TransactionCommand {

	private CommandReceiver cr;

	public OOCCommand() {
		this.cr = cr.getHandle();
	}

	public void execute() {
		cr.methOOC();
	}
}
