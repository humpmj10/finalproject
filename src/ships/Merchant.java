package ships;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import Galaxy.Planet;
import Items.*;
import Model.Controller;

/**
 * The Merchant object is a relatively common MOB that will trade with the
 * player, occasionally initiating trade-related interaction with the player. It
 * never attacks, and therefore has high defense and low offense. It moves
 * relatively slowly from planet to planet.
 * 
 * The setInventory() method is also called when the Merchant object is
 * constructed. The third argument in the constructor, the "tier", determines
 * what quality items this particular merchant carries.
 * 
 * @author Connor Lewis
 * 
 */
public class Merchant extends ActingMob implements Serializable {

	public ArrayList<Item> inventory;

	public Merchant(String title, int level, int[] route) {
		// the MOB stopover at each planet is randomly calculated to be between
		// 60 and 90 seconds (not final)
		super(title, route, 90000 - (new Random()).nextInt(30001));

		this.setAwareness(80);

		inventory = new ArrayList<Item>();
		this.setInventory(level);

		this.setStats(new Hull(MAX_ITEM_LEVEL - 1), new Weapon(2), new Shield(
				MAX_ITEM_LEVEL));
	}

	/**
	 * Assigns various items to the merchant's inventory.
	 */
	private void setInventory(int lvl) {

		inventory.add(new Fuel());
		inventory.add(new Drill());
		inventory.add(new ShieldSpecialist());
		inventory.add(new WeaponSpecialist());
		inventory.add(new Hull(lvl));
		inventory.add(new Hull(lvl + 1));
		inventory.add(new Weapon(lvl));
		inventory.add(new Weapon(lvl + 1));
		inventory.add(new Shield(lvl));
		inventory.add(new Shield(lvl + 1));
		inventory
				.add(new Artefact("Spice",
						"A tasty yet carcinogenic mold extract, highly prized among certain pilots."));
	}

	@Override
	protected synchronized void perform(Player player) {

		String message = "The merchant ship " + this.name
				+ " has sent you a message: \"Come shop at " + this.name
				+ "'s!\"";
		Controller.getInstance().append(player, message);
	}

	@Override
	public synchronized void respond(Player player) {

		String inventoryPrintOut = this.inventoryToString();
		Controller.getInstance().append(player, inventoryPrintOut);

	}

	private String inventoryToString() {

		String toString = super.name + "'s Inventory\n";

		for (int i = 0; i < inventory.size(); i++) {
			Item item = inventory.get(i);
			toString += "Item: " + item.getName() + "\tCost: "
					+ (3 * item.getValue() / 2) + "\n";
		}

		toString += "If you would like to purchase any of these wonderful items, enter \"Get <item name> from "
				+ this.name + "\". If you would like to sell one of your items, enter \"Give <item name> to "
				+ this.name + "\".";

		return toString;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		return "The merchant ship " + name + " drifts through "
				+ this.currentPlanet.getName() + "'s orbit.";
	}

	/**
	 * Returns a description of the ship and its behavior.
	 */
	@Override
	public String getDescription() {

		return this.name
				+ " is a traveling trading ship. You can engage it to see its "
				+ "inventory, and \"get\" an item from it to purchase (at "
				+ "albeit inflated costs).";
	}
}