package ships;

import java.io.Serializable;

import Items.Hull;
import Items.Shield;
import Items.Weapon;
import Model.Controller;

/**
 * The Thief is a fasting-moving but weak NPC ship that steal small amounts of
 * player cash. Upon being attacked a small amount of loot can be recovered from
 * the Thief.
 * 
 * @author Connor James Lewis
 * 
 */
public class Thief extends ActingMob implements Serializable {

	public Thief(String title, int[] route) {
		super(title, route, 18000);

		this.setAwareness(100);

		this.setStats(new Hull(2), new Weapon(1), new Shield(2));
	}

	/**
	 * The Thief steals a small amount of money somewhat proportional to the
	 * amount the player has on them.
	 */
	@Override
	protected synchronized void perform(Player player) {

		int playerWallet = player.getWallet();
		int theft = 0;

		if (playerWallet <= 0) {
			return;
		} else if (playerWallet <= 40) {
			theft = 5;
		} else if (playerWallet <= 80) {
			theft = 10;
		} else {
			theft = 20;
		}

		player.subtractCurrency(theft);
		if (theft > 0) {
			Controller.getInstance().append(player,
					this.name + " has stolen " + theft + " credits from you!");
		}
		
		return;
	}

	/**
	 * If the player tries to engage the Thief, the Thief will be attacked.
	 */
	@Override
	public synchronized void respond(Player player) {

		Controller.getInstance().processCombat(player, this);
	}

	/**
	 * 
	 */
	@Override
	public String toString() {

		return "The small thieving ship " + name + " darts around "
				+ currentPlanet.getName() + "'s orbit.";
	}

	/**
	 * Describes the ship, should the player "look at" it
	 */
	public String getDescription() {

		return name
				+ " is a small ship which, if it detects you, will steal a small amount"
				+ " of currency. It is relatively weak in combat but moves from planet to "
				+ "planet quickly.";
	}
}
