package ships;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import Items.*;
import Model.Controller;
import Galaxy.*;

/**
 * This is considered the player object for our game. It will contain the
 * players inventory, username/password, fuel and stats.
 * 
 * @author Mike Humphrey, Brian Smith. Connor Lewis, Brent Murdock
 * 
 */
public class Player extends Ship implements Serializable {

	private static final long serialVersionUID = 1L;

	private String password;
	private ArrayList<Item> inventory;
	private ArrayList<Item> equipped;
	private boolean loggedIn;
	private int currency;

	public Player(String userName, String password) {
		super(userName);
		this.name = userName;
		this.password = password;
		loggedIn = false;
		currency = 40;
		inventory = new ArrayList<Item>();
		equipped = new ArrayList<Item>();

		inventory.add(new Fuel());
		inventory.add(new Fuel());

		inventory.add(new Drill());
		inventory.add(new Drill());
		inventory.add(new Drill());

		inventory.add(new Minerals(1));
		inventory.add(new Metals(1));

		inventory.add(new Artefact("Strange Orb",
				"A strange alien orb that hums unnervingly."));

		equipped.add(new Hull(1));
		equipped.add(new Shield(1));
		equipped.add(new Weapon(1));

		// player starts out in one of the four central planets, randomly
		// selected
		Random random = new Random();
		int[] possibleStarting = { 14, 15, 20, 21 };
		int currentIndex = random.nextInt(4);
		Controller controller = Controller.getInstance();
		this.setPlanet(controller.getGalaxy().getPlanetByIndex(
				possibleStarting[currentIndex]));
		controller.getGalaxy().getPlanetByIndex(possibleStarting[currentIndex])
				.dropPlayerIntoOrbit(this);

		this.setStats(new Hull(1), new Weapon(1), new Shield(1));
	}

	/**
	 * Return the password for this ship
	 * 
	 * @return String password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Set the password for this ship/account
	 * 
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the player's current inventory.
	 * 
	 * @return arraylist<Item> list of items
	 */
	public ArrayList<Item> getInventory() {
		return inventory;
	}

	/**
	 * Gets list of upgrades the player's ship has equipped.
	 * 
	 * @return list of equipped upgrades
	 */
	public ArrayList<Item> getEquipped() {

		return equipped;
	}

	/**
	 * Get the toString representation of stats.
	 * 
	 * @return String the list of stats
	 */
	public String statsToString() {
		String result = "";
		result += "Health: " + stats.get("health") + "\n";
		result += "Attack: " + stats.get("attack") + "\n";
		result += "Defense: " + stats.get("defense") + "\n";

		return result;
	}

	/**
	 * A specified item is purchased from a specified merchant. If the merchant
	 * doesn't have the requested item in inventory, returns an error; else,
	 * transaction occurs: money is lost and the item is added to the player
	 * inventory.
	 * 
	 * @param itemRequested
	 *            the item being bought
	 * @return true if the transaction was successful, false otherwise
	 */
	public synchronized boolean purchase(Merchant merchant,
			String nameOfRequested) {

		ArrayList<Item> merchantInventory = merchant.inventory;
		Item itemInStock = null;

		for (Item item : merchantInventory) {
			if (nameOfRequested.toLowerCase().startsWith(
					item.getName().toLowerCase())) {
				itemInStock = item;
				break;
			}
		}

		if (itemInStock == null) {
			Controller.getInstance().append(
					this,
					"Sorry, " + merchant.getName()
							+ " doesn't have that item in stock!");
			return false;
		}

		if (itemInStock.getValue() > currency) {
			Controller.getInstance().append(
					this,
					"You only have " + currency
							+ " credits. You cannot afford a "
							+ itemInStock.getName() + " at this time!");
			return false;
		} else {

			int overPriced = 3 * itemInStock.getValue() / 2;
			subtractCurrency(overPriced);
			inventory.add(itemInStock);
			Controller.getInstance().append(
					this,
					"You have spent " + overPriced + " credits and received a "
							+ itemInStock.getName() + ".");

			return true;
		}
	}

	/**
	 * The player sells a specified item in their inventory. If there is no such
	 * item in inventory, they receive an error message; otherwise, they
	 * successfully sell it at 75% the item's actual value.
	 * 
	 * @param item
	 *            the item being sold
	 * @return true if sold, false otherwise
	 */
	public synchronized boolean sell(Item itemRequested) {

		Item itemToSell = null;

		String nameOfRequested = itemRequested.getName().toLowerCase();

		for (Item item : inventory) {
			if (nameOfRequested.startsWith(item.getName().toLowerCase())) {
				itemToSell = item;
				break;
			}
		}

		// sell item at 75% its actual value
		int profit = (itemToSell.getValue() * 3) / 4;
		inventory.remove(itemToSell);
		this.addCurrency(profit);

		Controller.getInstance().append(
				this,
				"You have received " + profit + " credits for selling one "
						+ itemRequested.getName() + ".");

		return true;
	}

	/**
	 * Decrements the player's currency by the specified amount; used when
	 * purchasing, or stolen/taken after combat loss.
	 * 
	 * @param amount
	 *            the amt to subtract
	 */
	public synchronized void subtractCurrency(int amount) {
		currency -= amount;
	}

	public synchronized void addCurrency(int increase) {
		currency += increase;
	}

	/**
	 * Returns the amount of currency (or "credits") the player currently has.
	 * 
	 * @return amt of money player has
	 */
	public int getWallet() {

		return currency;
	}

	/**
	 * 
	 * @param direction
	 * @return
	 */
	public synchronized boolean move(Direction direction) {

		Planet planet = currentPlanet.getNeighbor(direction);

		return super.move(planet);
	}

	/**
	 * 
	 */
	@Override
	public String toString() {

		return "A player, " + name + ", orbits this planet.";
	}

	/**
	 * Gives another player's stats upon this player's request.
	 */
	@Override
	public String getDescription() {
		return this.name + ": Player Stats: \n" + this.statsToString();
	}

	/**
	 * 
	 * @return True if player is logged in
	 */
	public boolean isLoggedIn() {
		return loggedIn;
	}

	/**
	 * 
	 * @param loggedIn
	 *            Set loggedIn to false/true
	 */
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;

	}

	/**
	 * Player uses a usable item in the inventory, which may have one of several
	 * effects.
	 * 
	 * @param item
	 */
	public void useItem(Item usable) {

		if (!(usable instanceof Usable)) {
			Controller.getInstance().append(this,
					"The " + usable.getName() + " is not a usable item!");
			return;
		}

		Item toUse = null;

		if (usable instanceof WeaponSpecialist) {

			for (Item item : inventory) {
				if (item instanceof WeaponSpecialist) {
					toUse = item;
					break;
				}
			}

			Weapon equippedWeapon = null;

			for (Item equippedItem : equipped) {
				if (equippedItem instanceof Weapon) {
					equippedWeapon = (Weapon) equippedItem;
				}
			}

			int currentTier = equippedWeapon.getUpgradeTier();
			if (currentTier == 5) {
				Controller
						.getInstance()
						.append(this,
								"Your weapon is already top-knotch. The specialist commits suicide due to his inability to improve upon it.");
			} else {
				Weapon replacement = new Weapon(currentTier+1);
				equipped.remove(equippedWeapon);
				equipped.add(replacement);
				Controller.getInstance().append(
						this,
						"The specialist has upgraded your "
								+ equippedWeapon.getName() + " to a "
								+ replacement.getName() + ".");
				
				Hull hull = null;
				Shield shield = null;
				for(Item item : equipped) {
					if (item instanceof Hull) {
						hull = (Hull) item;
					} else if (item instanceof Shield) {
						shield = (Shield) item;
					}
				}
				
				this.setStats(hull, replacement, shield);
			}
		}

		if (usable instanceof ShieldSpecialist) {

			for (Item item : inventory) {
				if (item instanceof ShieldSpecialist) {
					toUse = item;
					break;
				}
			}

			Shield equippedShield = null;

			for (Item equippedItem : equipped) {
				if (equippedItem instanceof Shield) {
					equippedShield = (Shield) equippedItem;
				}
			}

			int currentTier = equippedShield.getUpgradeTier();
			if (currentTier == 5) {
				Controller
						.getInstance()
						.append(this,
								"Your shield is already top-knotch. The specialist commits suicide due to his inability to improve upon it.");
			} else {
				Shield replacement = new Shield(currentTier + 1);
				equipped.remove(equippedShield);
				equipped.add(replacement);
				Controller.getInstance().append(
						this,
						"The specialist has upgraded your "
								+ equippedShield.getName() + " to a "
								+ replacement.getName() + ".");
				
				Hull hull = null;
				Weapon weapon = null;
				for(Item item : equipped) {
					if (item instanceof Hull) {
						hull = (Hull) item;
					} else if (item instanceof Weapon) {
						weapon = (Weapon) item;
					}
				}
				
				this.setStats(hull, weapon, replacement);
			}
		}

		if (usable instanceof Drill) {

			for (Item item : inventory) {
				if (item instanceof Drill) {
					toUse = item;
					break;
				}
			}

			ArrayList<Resource> resourcesToMine = this.currentPlanet
					.getResources();
			// randomly selects which resource will be mined
			Random rand = new Random();
			int indexResource = rand.nextInt(resourcesToMine.size());
			Resource resourceMined = resourcesToMine.get(indexResource);
			Controller.getInstance().append(
					this,
					"You have used one drill bit to mine "
							+ resourceMined.getName() + " from "
							+ currentPlanet.getName() + ".");
			inventory.add(resourceMined);
		}

		if (usable instanceof Fuel) {
			Controller
					.getInstance()
					.append(this,
							"The Fuel tank is used automatically. You have no use for it now.");

		}
		if (toUse != null) {
			inventory.remove(toUse);
		} else {
			Controller.getInstance().append(this,
					"You do not have that item in your inventory!");
		}
	}
}
