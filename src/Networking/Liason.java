package Networking;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import ships.Player;
import Model.Controller;

/**
 * Each instance of this class allows a client to interact with the database.
 * Currently the functionality is to allow a client to get added to the
 * collection of concurrently connected clients, to quit which removes them from
 * the database, and to also get a list of all people's names who are currently
 * connected. The client can enter these three requests for services:
 * 
 * @author Michael Humphrey and Brian Smith
 * 
 */

public class Liason implements Runnable {
	// Server related objects
	private Socket socketFromServer;
	private ObjectInputStream readFromClient;
	private ObjectOutputStream writeToClient;

	// Game/user related objects
	private Vector<Liason> sharedPlayersLoggedIn;
	private ConcurrentHashMap<Player, Liason> liasonsByPlayer;
	private ClientList userBase;
	private Controller c;
	private Player me;
	private Server server;

	/**
	 * Construct an object that will run in it's own Thread so this object can
	 * communicate with that one connection. When this client quits, all message
	 * that client entered are removed from the sharedCollectionReference.
	 */
	public Liason(Socket socketFromServer,
			Vector<Liason> sharedCollectionReference,

			ConcurrentHashMap<Player, Liason> lbp, Server server) {
		this.socketFromServer = socketFromServer;
		this.sharedPlayersLoggedIn = sharedCollectionReference;
		c = Controller.getInstance();
		this.userBase = c.getPlayerBase();
		liasonsByPlayer = lbp;
		this.server = server;
	}

	@Override
	public void run() {
		// Open the input and output streams so the
		// client can interact with the collection
		try {
			readFromClient = new ObjectInputStream(
					socketFromServer.getInputStream());
			writeToClient = new ObjectOutputStream(
					socketFromServer.getOutputStream());
		} catch (IOException e) {
			System.out
					.println("Exception thrown while obtaining input & output streams");
			e.printStackTrace();
			return;
		}

		String messageFromClient = null;
		boolean wantToAStayConnected = true;

		// Loop as long as we are connected using a boolean variable
		while (wantToAStayConnected) {
			try {
				messageFromClient = (String) readFromClient.readObject();
			} catch (IOException e1) {
				System.out
						.println("IOException in ServerThread.run when reading from client");
				return;
			} catch (ClassNotFoundException e1) {
				System.out
						.println("ClassCastException in ServerThread.run casting object from client to String");
				e1.printStackTrace();
			}

			// Now that we have read a valid string from Client, process it:
			try { // Process a "put Name"

				if (messageFromClient.length() > 3
						&& messageFromClient.substring(0, 3).equals("/c ")) {
					// parsing string into username and command to pass to Controller.
					String message = messageFromClient.substring(3);
					String[] parsed = message.split("/:/");
					Player player = userBase.getPlayer(parsed[0]);
					Controller.getInstance().readCommand(player, parsed[1]);
				}

				if (messageFromClient.length() > 3
						&& messageFromClient.substring(0, 3).equals("/a ")) {
					// parsing string into username and password to create Player acct in Server.
					String message = messageFromClient.substring(3);
					String[] parsed = message.split("/:/");
					Player player = new Player(parsed[0], parsed[1]);
					if (userBase.addUser(parsed[0], player)) {
						c.savePlayerBase();
						writeToClient
								.writeObject("You have successfully created an account.");
					} else
						writeToClient
								.writeObject("The username you selected is already in use.");
				}

				if (messageFromClient.length() > 3
						&& messageFromClient.substring(0, 3).equals("/l ")) {
					// parsing string into username and password to check if user is logged in.
					String message = messageFromClient.substring(3);
					String[] parsed = message.split("/:/");

					if (userBase.checkPassword(parsed[0], parsed[1])) {
						me = userBase.getPlayer(parsed[0]);
						me.setLoggedIn(true);
						liasonsByPlayer.put(me, this);
						writeToClient.writeObject("/l true");
						c.readCommand(me, "look");
					} else {
						writeToClient.writeObject("/l false");
					}
				}

				if (messageFromClient.length() >= 3
						&& messageFromClient.substring(0, 3).equalsIgnoreCase(
								"ooc")) {
					String clientMessage = messageFromClient.substring(4)
							.trim();
					for (int i = 0; i < sharedPlayersLoggedIn.size(); i++) {
						Liason temp = sharedPlayersLoggedIn.get(i);
						temp.writeToClient.reset();
						temp.writeToClient.writeObject("(Global Chat)"
								+ clientMessage);
						temp.writeToClient.flush();
					}
				}
				if (messageFromClient.length() >= 3
						&& messageFromClient.substring(0, 3).equalsIgnoreCase(
								"say")) {
					String clientMessage = messageFromClient.substring(4)
							.trim();
					Set<Player> s = liasonsByPlayer.keySet();
					for (Player p : s) {
						if (p.getPlanet().equals(me.getPlanet())) {
							Liason temp = liasonsByPlayer.get(p);
							temp.writeToClient.reset();
							temp.writeToClient.writeObject("(Planet Chat)"
									+ me.getName() + " says: "
									+ clientMessage);
							temp.writeToClient.flush();
						}
					}

				}
				if (messageFromClient.length() >= 5
						&& messageFromClient.substring(0, 5).equalsIgnoreCase(
								"/who/")) {
					String message = "The following Players are logged in:"
							+ "\n";
					if (!sharedPlayersLoggedIn.isEmpty()) {
						Set<Player> s = liasonsByPlayer.keySet();
						for (Player p : s) {
							message += p.getName() + "\n";
						}
						writeToClient.reset();
						writeToClient.writeObject(message);
						writeToClient.flush();
					}

				}
				if (messageFromClient.length() >= 6
						&& messageFromClient.substring(0, 6).equalsIgnoreCase(
								"/shut/")) {
					server.exit();
				}
				if (messageFromClient.length() >= 4
						&& messageFromClient.substring(0, 4).equalsIgnoreCase(
								"tell")) {
					Scanner scan = new Scanner(messageFromClient);
					scan.next();
					String username = scan.next().toLowerCase();
					String sender = scan.next().toLowerCase();
					String message = sender + " whipsers to you: ";
					while (scan.hasNext()) {
						message += scan.next() + " ";
					}
					Set<Player> s = liasonsByPlayer.keySet();
					boolean flag = false;
					Player player = null;
					for (Player p : s) {
						if (p.getName().equalsIgnoreCase(username)) {
							flag = true;
							player = p;
						}
					}
					if (flag) {
						Liason temp = liasonsByPlayer.get(player);
						temp.writeToClient.reset();
						temp.writeToClient.writeObject(message.trim());
						temp.writeToClient.flush();
						
					} else {
						if (userBase.playerExists(username)) {
							this.writeToClient.reset();
							this.writeToClient
									.writeObject("That player isn't logged in");
							this.writeToClient.flush();
						} else {
							this.writeToClient.reset();
							this.writeToClient
									.writeObject("That player doesn't exist");
							this.writeToClient.flush();
						}

					}
				}

				if (messageFromClient.equalsIgnoreCase("quit")) { // Process a "quit"
					// Clean up and avoid exceptions
					sharedPlayersLoggedIn.remove(this);
					if (me != null)
						liasonsByPlayer.remove(me);
					me.setLoggedIn(false);
					socketFromServer.close();
					readFromClient.close();
					writeToClient.close();
					wantToAStayConnected = false;
					c.savePlayerBase();

				}

			} catch (Exception e) {
				if (me != null) {
					liasonsByPlayer.remove(me);
					me.setLoggedIn(false);
				}	
				e.printStackTrace();
				wantToAStayConnected = false;
			}
		}

	}

	public synchronized void sendMessageToClient(String message) {

		try {
			writeToClient.writeObject(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}