package Commands;

import java.util.ArrayList;

import ships.Player;

/**
 * 
 * @author Michael Humphrey
 * 
 */
public class CommandArgument {

	private Object[] args;

	public CommandArgument() {
		args = new Object[2];
	}

	/**
	 * Get the argument array.
	 * @return Object[]
	 */
	public Object[] getArguments() {
		return args;
	}

	/**
	 * 
	 * @param i1
	 * @param i2
	 */
	public void setArgument(int i1, int i2) {
		args[0] = i1;
		args[1] = i2;
	}

	/**
	 * This constructor is used for createAccount Command
	 * 
	 * @param username
	 * @param password
	 */
	public void setArgument(String username, String password) {
		args[0] = username;
		args[1] = password;
	}
	
	/**
	 * Used to add an inventory list to the command
	 * @param items
	 */
	public void setArgument(ArrayList<String> items) {
		args[0] = items;
	}
	
	public void setArgument(Player me) {
		args[0] = me;
	}
}
