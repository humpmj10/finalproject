package Items;

import java.io.Serializable;

/**
 * This subclass of Item functions as a superclass for the types of item such as
 * Fuel Tanks and Repair Kits that change stats or otherwise affect gameplay
 * when used, but do not alter it permanently. Usable items are consumed after
 * use.
 * 
 */
public abstract class Usable extends Item implements Serializable {

	public Usable() {
		super();

	}

	/**
	 * Describes the item's effect upon use.
	 * 
	 * @return
	 */
	public abstract String getDescription();
}
