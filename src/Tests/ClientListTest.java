package Tests;

import static org.junit.Assert.*;
import static org.junit.Assert.*;

import org.junit.Test;

import ships.Player;

import Networking.ClientList;

/**
 * This is a test class for clientlist which is responsible for holding all the
 * accounts that have been created.
 * 
 * @author Michael Humphrey
 * 
 */
public class ClientListTest {

	/**
	 * Test checkPassword
	 */
	@Test
	public void testCheckPassword() {
		ClientList players = new ClientList();
		String username = "mike";
		Player player = new Player(username, "bug");
		players.addUser(username, player);
		assertFalse(players.checkPassword(username, "bug1"));
		assertTrue(players.checkPassword(username, "bug"));
		assertEquals(player, players.getPlayer(username));
	}

	/**
	 * Test add/remove users for client list
	 */
	@Test
	public void testAddAndRemove() {
		ClientList players = new ClientList();
		assertFalse(players.playerExists("mIke"));
		assertFalse(players.playerExists("bOb"));
		Player player = new Player("Mike", "bug");
		Player player2 = new Player("Bob", "bug");
		assertTrue(players.addUser("Mike", player));
		assertTrue(players.addUser("Bob", player2));
		assertTrue(players.playerExists("Mike"));
		assertTrue(players.playerExists("MIke"));
		assertTrue(players.playerExists("MIKe"));
		assertTrue(players.playerExists("MIKE"));
		assertTrue(players.playerExists("bob"));
		assertFalse(players.addUser("Mike", player));
		assertFalse(players.deleteUser("Mike", "bug1"));
		assertTrue(players.deleteUser("Mike", "bug"));
	}

}
