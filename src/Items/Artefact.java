package Items;

import java.io.Serializable;

/**
 * An item that can be sold or given to Laboratory mobs for a quest.
 * 
 * @author Connor James Lewis
 * 
 */
public class Artefact extends Item implements Serializable {

	private String description;

	public Artefact(String name, String describe) {
		super();

		this.name = name;
		this.description = describe + " This artefact is worth 100 credits.";
		setValue();
	}

	@Override
	protected void setValue() {

		this.value = 100;
	}

	@Override
	public String getDescription() {

		return description;
	}

	protected void setName() {
	}

}
