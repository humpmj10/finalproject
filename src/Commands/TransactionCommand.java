package Commands;

import java.util.Vector;

/**
 * This is the class that implements class. Lower ActionCommand classes will
 * extend this class. This makes it easier to add new commands.
 * 
 * @author Michael Humphrey
 * 
 */
public class TransactionCommand implements Command {
	private CommandReceiver commandreceiver;
	private Vector<String> commandnamelist;
	private Vector<CommandArgument> commandargumentlist;
	private String commandname;
	private CommandArgument commandargument;
	private Command command;

	/**
	 * 
	 */
	public TransactionCommand() {
		this(null, null);
	}

	/**
	 * Used if arguments are needed
	 * 
	 * @param commandnamelist
	 * @param commandargumentlist
	 */
	public TransactionCommand(Vector<String> commandnamelist) {
		this.commandnamelist = commandnamelist;
		this.commandargumentlist = null;
		commandreceiver = CommandReceiver.getHandle();
	}

	/**
	 * Used if arguments are needed
	 * 
	 * @param commandnamelist
	 * @param commandargumentlist
	 */
	public TransactionCommand(Vector<String> commandnamelist,
			Vector<CommandArgument> commandargumentlist) {
		this.commandnamelist = commandnamelist;
		this.commandargumentlist = commandargumentlist;
		commandreceiver = CommandReceiver.getHandle();
	}

	/**
	 * Execute all commands currently in the list.
	 */
	public void execute() {
		for (int i = 0; i < commandnamelist.size(); i++) {
			commandname = (String) (commandnamelist.get(i));
			if (!commandargumentlist.isEmpty()) {
				commandargument = (CommandArgument) ((commandargumentlist
						.get(i)));
			}
			commandreceiver.setCommandArgument(commandargument);
			String classname = "Commands." + commandname + "Command";
			try {
				Class cls = Class.forName(classname);
				command = (Command) cls.newInstance();
			} catch (Throwable e) {
				System.err.println(e);
			}
			command.execute();
		}
	}
}