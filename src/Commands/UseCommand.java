package Commands;

/**
 * Command when player uses a specific item.
 * 
 * @author Michael Humphrey
 * 
 */
public class UseCommand extends TransactionCommand {

	private CommandReceiver cr;

	public UseCommand() {
		this.cr = cr.getHandle();
	}

	public void execute() {
		cr.methUseItem();
	}
}
