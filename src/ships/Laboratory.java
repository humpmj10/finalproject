package ships;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import Galaxy.Planet;
import Items.*;
import Model.Controller;

/**
 * The mobile Laboratory is a friendly Mob that offers a "quest": if the player
 * turns in 3 artefacts then they are rewarded with a cloaking device. Upon
 * engaging the Laboratory mob, these items are automatically removed from the
 * player's inventory, if present. The mob will also send a message to the
 * player notifying them of this opportunity.
 * 
 */
public class Laboratory extends ActingMob implements Serializable {

	public Laboratory(String title, int[] route) {
		// the MOB stopover at each planet is randomly calculated to be between
		// 30 and 60 seconds (not final)
		super(title, route, 60000 - (new Random()).nextInt(30001));

		this.setAwareness(150);

		this.setStats(new Hull(MAX_ITEM_LEVEL), new Weapon(1), new Shield(
				MAX_ITEM_LEVEL));
	}

	@Override
	protected void perform(Player player) {
		String message = "The mobile research lab "
				+ this.name
				+ " has sent you a message: \"We are looking for artefacts for our "
				+ "research! Bring us 3, and we will reward you with one of "
				+ "our high-tech Cloaking Devices!\"";
		Controller.getInstance().append(player, message);
	}

	/**
	 * Its response to friendly player engagement is to automatically fulfill
	 * the "quest".
	 */
	@Override
	public void respond(Player player) {

		Controller.getInstance().append(player, this.name + " is look for 3 artefacts.");
		this.giveArtefacts(player);
	}

	private synchronized void giveArtefacts(Player player) {

		ArrayList<Item> artefacts = new ArrayList<Item>();

		ArrayList<Item> inventory = player.getInventory();
		Item candidate;

		for (int i = 0; i < inventory.size(); i++) {
			candidate = inventory.get(i);
			if (candidate instanceof Artefact) {
				artefacts.add(candidate);
			}
		}

		if (artefacts.size() < 3) {
			Controller.getInstance().append(player,
					"Sorry, you do not have enough artefacts to trade with "
							+ this.name + ".");
		} else {

			for (int i = 0; i < 3; i++) {
				inventory.remove(artefacts.get(i));
			}

			Cloak cloak = new Cloak();
			inventory.add(cloak);
			Controller.getInstance().append(
					player,
					this.name
							+ " has sent you a message: \"Thank you for your gift of 3 artefacts! We have given you a cloaking device as a sign of goodwill.\"");
		}
	}

	/**
	 * 
	 */
	@Override
	public String toString() {

		return "The scientists' ship " + name + " drifts around "
				+ this.currentPlanet.getName() + ".";
	}

	/**
	 * Returns a description of the ship and its behavior.
	 */
	@Override
	public String getDescription() {

		return name
				+ " is a scientists' mobile research lab. Such ships explore "
				+ "the outer rim of the galaxy and will gladly trade a player's "
				+ "artefacts for one of their high-tech cloaking devices.";
	}
}