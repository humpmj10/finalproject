package ships;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import Galaxy.Planet;
import Items.*;

/**
 * Many Mobs initiate interaction with the player upon or shortly after entry
 * into the planet's orbit. This can be friendly or hostile. They are also
 * capable of more complex interactions than some other Mobs.
 * 
 * ActingMobs often take as an argument a "level" used to determine their
 * effectiveness in battle and other properties.
 * 
 * @author Connor Lewis
 * 
 */
public abstract class ActingMob extends Mob implements Serializable {

	protected int awareness;

	protected ActingMob(String title, int[] route, int timeInSystem) {
		super(title, route, timeInSystem);

	}

	/**
	 * When constructing a MOB, assigns it an awareness level (out of 100). Upon
	 * entering the system, the player may or may not be noticed by the MOB,
	 * based on its awareness level and their own stats. This determines whether
	 * or not a hostile MOB will attack, or a friendly MOB initiate
	 * communication.
	 */
	protected void setAwareness(int aware) {

		awareness = aware;
	}

	/**
	 * ActingMobs sometimes initiate actions toward the player. These may be
	 * aggressive or friendly, depending on the nature of the Mob, and varies
	 * specifically by type.
	 */
	public void act(Player player) {

		if (detect(player)) {
			
			perform(player);
		}
	}

	/**
	 * If the mob successfully detects the player's ship, its particular action
	 * is taken.
	 */
	protected abstract void perform(Player player);

	/**
	 * Returns whether or not the Mob successfully detects the player. If the
	 * player has acquired and installed the cloaking device, detection is made
	 * more difficult.
	 * 
	 * @return true if the mob senses the player's ship, false otherwise
	 */
	protected boolean detect(Player player) {

		int range = 101;

		// having a cloaking device equipped increases the player's chances of
		// avoiding detection
		ArrayList<Item> equipped = player.getEquipped();
		for (Item item : equipped) {
			if (item instanceof Cloak) {
				range += 50;
				break;
			}
		}

		Random random = new Random();
		int concealRoll = random.nextInt(range);

		if (concealRoll <= awareness) {
			return true;
		} else {
			return false;
		}
	}

}
