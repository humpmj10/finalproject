package Items;

import java.io.Serializable;

public class Drill extends Usable implements Serializable {

	public Drill() {
		super();
		
		this.setName();
		this.setValue();
	}

	@Override
	protected void setName() {
		this.name = "Drill Bit";

	}

	@Override
	protected void setValue() {
		this.value = 15;
	}

	@Override
	public String getDescription() {

		return "The drill bit is a one-use item. If you use a drill bit, you will automatically mine a random resource from the planet you are currently orbiting. It is worth "
				+ this.value + " credits.";
	}
}