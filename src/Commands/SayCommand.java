package Commands;

import ships.Player;

/**
 * Executes a say command. This is a planet chat function, so all players in the
 * planet's area will be broadcasted this message.
 * 
 * @author Michael Humphrey
 * 
 */
public class SayCommand extends TransactionCommand {

	private CommandReceiver cr;

	/**
	 * Set the player for this command
	 * 
	 */
	public SayCommand() {
		this.cr = cr.getHandle();
	}

	public void execute() {
		cr.methSay();
	}

}
