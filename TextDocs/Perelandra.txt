	Perelandra is dangerous. It's in the name. It's a planet of fiery,
burning volcanoes. During the day it suffers heavy atmospheric storms
of acid and it burns at over 1000 degrees centigrade. At night,
the atmosphere almost disappears, and the planet is bombarded with
meteors and is as cold as Pluto. So naturally no one lives there.	
	