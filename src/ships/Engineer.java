package ships;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import Galaxy.Planet;
import Items.*;
import Model.Controller;

/**
 * The Engineer mob installs upgrades for the player, once the player has
 * acquired them. The player cannot equip upgrades without the help of an
 * engineer. The engineer is always friendly (unless attacked) and its stats are
 * defense-centric rather than offense-centric.
 * 
 * @author Connor Lewis
 * 
 */
public class Engineer extends Mob implements Serializable {

	ArrayList<Item> inventory;

	public Engineer(String title, int[] route) {
		// the MOB stopover at each planet is randomly calculated to be between
		// 60 and 90 seconds (not final)
		super(title, route, 90000 - (new Random()).nextInt(30001));

		this.setStats(new Hull(MAX_ITEM_LEVEL - 1), new Weapon(2), new Shield(
				MAX_ITEM_LEVEL - 1));
	}

	@Override
	public synchronized void respond(Player player) {

		Controller
				.getInstance()
				.append(player,
						this.name
								+ " has sent a message: \"Let\'s see if we can help you install "
								+ "your highest-quality upgrades, for the very low price of 1/3 "
								+ "the upgrade's value!\"");
		if (replace(player.getInventory(), player.getEquipped())) {
			Controller.getInstance().append(player,
					"Upgrades were successfully installed! Your stats have been updated accordingly.");

			Hull hull = null;
			Weapon weapon = null;
			Shield shield = null;
			for(Item item : player.getEquipped()) {
				if (item instanceof Hull) {
					hull = (Hull) item;
				} else if (item instanceof Weapon) {
					weapon = (Weapon) item;
				} else if (item instanceof Shield) {
					shield = (Shield) item;
				}
			}
			
			player.setStats(hull, weapon, shield);
		} else {
			Controller.getInstance().append(player,
					"You have no superior upgrades to install.");
		}

	}

	private synchronized boolean replace(ArrayList<Item> inventory,
			ArrayList<Item> equipped) {

		boolean status = false;
		@SuppressWarnings("unchecked")
		ArrayList<Item> inventoryCopy = (ArrayList<Item>) inventory.clone();

		for (Item currentUpgrade : inventoryCopy) {

			@SuppressWarnings("unchecked")
			ArrayList<Item> equippedCopy = (ArrayList<Item>) equipped.clone();
			for (Item replaceEquipment : equippedCopy) {

				@SuppressWarnings("rawtypes")
				Class replacementUpgradeClass = replaceEquipment.getClass();

				if (replacementUpgradeClass.isInstance(currentUpgrade)) {
					if (((TieredUpgrade) currentUpgrade).getUpgradeTier() > ((TieredUpgrade) replaceEquipment)
							.getUpgradeTier()) {
						equipped.remove(replaceEquipment);
						inventory.add(replaceEquipment);
						equipped.add(currentUpgrade);
						inventory.remove(currentUpgrade);
						status = true;
					}
				}
			}
		}
		
		ArrayList<Item> equippedCopy = (ArrayList<Item>) equipped.clone();
		
		Cloak cloak = null;
		for(Item item : inventoryCopy) {
			if(item instanceof Cloak) {
				cloak = (Cloak) item;
				break;
			}
		}
		boolean cloakAlreadyEquipped = false;
		if(cloak != null) {
			for(Item item : equippedCopy) {
				if(item instanceof Cloak) {
					cloakAlreadyEquipped = true;
				}
			}
			if(!cloakAlreadyEquipped) {
				inventory.remove(cloak);
				equipped.add(cloak);
				status = true;
			}
		}
		
		return status;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {

		return "The engineering vessel " + name + " orbits "
				+ this.currentPlanet.getName() + " slowly.";
	}

	/**
	 * Returns a description of the police ship.
	 */
	@Override
	public String getDescription() {

		return name
				+ " is an engineering ship. If you engage it, it will install the highest upgrades"
				+ " in your inventory if they are higher than the ones you already have equipped.";
	}
}