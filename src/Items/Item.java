package Items;

import java.io.Serializable;

public abstract class Item implements Serializable {

	protected String name;
	protected int value;


	public Item() {
		
	}

	/**
	 * The name is automatically set within the constructor of each type.
	 */
	protected abstract void setName();

	/**
	 * Returns the Item's name (for inventory and trading purposes), once it has
	 * already been assigned.
	 * 
	 * @return the item's name
	 */
	public synchronized String getName() {
		return name;
	}

	/**
	 * The value is automatically set within the constructor and is
	 * type-dependent.
	 */
	protected abstract void setValue();

	/**
	 * Returns the amount of credits the item could sell for
	 * 
	 * @return the number of credits it's worth
	 */
	public int getValue() {
		return value;
	}

	/**
	 * What the player sees when he "looks at" the item.
	 * @return
	 */
	public abstract String getDescription();

}
