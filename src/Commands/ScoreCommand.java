package Commands;

/**
 * This command will lists all the player's stats.
 * 
 * @author Michael Humphrey
 * 
 */
public class ScoreCommand extends TransactionCommand {

	private CommandReceiver cr;

	public ScoreCommand() {
		this.cr = cr.getHandle();
	}

	public void execute() {
		cr.methScore();
	}
}
