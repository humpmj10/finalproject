package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Galaxy.Direction;
import Galaxy.Wall;

public class WallTest {

	private Wall wall;

	@Test
	public void testWallCorners() {
		wall = wall.getInstance();
		assertTrue(wall.isActive(Direction.NORTH, 0, 0));
		assertTrue(wall.isActive(Direction.WEST, 0, 0));
		assertTrue(wall.isActive(Direction.NORTH, 0, 5));
		assertTrue(wall.isActive(Direction.EAST, 0, 5));
		assertTrue(wall.isActive(Direction.SOUTH, 5, 0));
		assertTrue(wall.isActive(Direction.WEST, 5, 0));
		assertTrue(wall.isActive(Direction.SOUTH, 5, 0));
		assertTrue(wall.isActive(Direction.WEST, 5, 0));
		assertTrue(wall.isActive(Direction.SOUTH, 5, 5));
		assertTrue(wall.isActive(Direction.EAST, 5, 5));
	}

}
