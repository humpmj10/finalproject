package Commands;

/**
 * Command when player wishes to get inventory.
 * 
 * @author Michael Humphrey
 * 
 */
public class InventoryCommand extends TransactionCommand {

	private CommandReceiver cr;

	public InventoryCommand() {
		this.cr = cr.getHandle();
	}

	public void execute() {
		cr.methGetInventory();
	}
}
