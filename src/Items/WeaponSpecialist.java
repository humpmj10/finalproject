package Items;

import java.io.Serializable;

/**
 * The Weapon Specialist is a Usable object whose effect is to replace a
 * player's equipped weapon with a weapon one tier higher. If already the
 * highest, will give the user a message.
 * 
 * @author Connor James Lewis
 * 
 */
public class WeaponSpecialist extends Usable implements Serializable {

	public WeaponSpecialist() {
		super();

		setName();
		setValue();
	}

	@Override
	public String getDescription() {

		return "You can \"use\" the Weapon Specialist to permanently improve your weapon by one tier. Hiring the specialist costs "
				+ this.value + " credits.";
	}

	@Override
	protected void setName() {
		this.name = "Weapon Specialist";
	}

	@Override
	protected void setValue() {
		this.value = 55;
	}
}
