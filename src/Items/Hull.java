package Items;

import java.io.Serializable;

/**
 * The Hull upgrade increases a ship's health points and is useful in combat.
 * The Hull upgrade increases a ship's health points.
 * 
 */
public class Hull extends TieredUpgrade implements Serializable {

	public Hull(int itemTier) {
		super(itemTier);

		if (itemTier > 5) {
			this.tier = 5;
		} else if (itemTier < 1) {
			this.tier = 1;
		} else {
			this.tier = itemTier;
		}

		this.setName();
		this.setValue();

	}

	@Override
	protected void setName() {

		String[] tiers = { "Wooden Siding", "Tin Hull", "Iron Hull",
				"Steel Hull", "Palladium Shell" };
		name = tiers[this.tier - 1];

	}

	@Override
	protected void setValue() {
		int[] tiers = { 5, 15, 20, 25, 40 };
		this.value = tiers[this.tier - 1];
	}

	@Override
	public String getDescription() {
		String[] tiers = {
				"The Wooden Siding is the weakest, cheapest type of hull.",
				"The Tin Hull is slightly better than the Wooden Siding, but is still very weak and accordingly cheap.",
				"The Iron Hull is a mid-range hull offering decent protection at a decent price.",
				"The Steel Hull is an upper-range hull that improves upon the Iron Hull.",
				"The Palladium Shell is the most expensive and most protective hull available." };

		return tiers[tier - 1] + " This hull is worth " + this.getValue() + " credits.";
	}

}
