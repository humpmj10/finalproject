package Commands;

import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import ships.Player;
import views.ClientGUI;
import Model.Controller;

/**
 * Handles the commands as they are issued in order. Contains the methods that
 * each command executes when execute is called on them. Uses the singleton
 * pattern so that only one CommandReciever can exists and commands will always
 * be executed in order.
 * 
 * @author Michael Humphrey
 * 
 */
public final class CommandReceiver {
	private Object[] command;
	private CommandArgument argument;
	private Controller controller;
	private JTextField entry;
	private ClientGUI GUI;

	private CommandReceiver() {
		command = new Object[2];
		controller = controller.getInstance();
	}

	private static CommandReceiver cr = new CommandReceiver();

	public static CommandReceiver getHandle() {
		return cr;
	}

	/**
	 * Needed to pass along the entry field from the gui
	 * 
	 * @param entry
	 */
	public void setEntry(JTextField entry) {
		this.entry = entry;
	}

	/**
	 * Needed to pass along the entry field from the gui
	 * 
	 * @param entry
	 */
	public void setGUI(ClientGUI GUI) {
		this.GUI = GUI;
	}

	/**
	 * Used to set commandargument
	 * 
	 * @param argument
	 */
	public void setCommandArgument(CommandArgument argument) {
		this.argument = argument;
	}

	/**
	 * Used by command object to create account.
	 */
	public void methCreateAccount() {
		String username = (String) JOptionPane.showInputDialog(entry,
				"Enter your username:\n"
						+ "\"Press Cancel if you don't want to create one\"",
				"Username Picker", JOptionPane.PLAIN_MESSAGE, null, null, null);
		String password = (String) JOptionPane.showInputDialog(entry,
				"Enter your password:\n"
						+ "\"Press Cancel if you don't want to create one\"",
				"Password Picker", JOptionPane.PLAIN_MESSAGE, null, null, null);
		if (username != null && password != null & !username.equals("")
				& !password.equals("")) { //make sure an entry was inputed and not blank
			Player newPlayer = new Player(username, password);
			controller.getPlayerBase().addUser(username, newPlayer);
			GUI.clearTextInput();
			GUI.addTextToGame("You have successfuly created an account "
					+ username);
		} else { // player did not input or cancelled
			GUI.addTextToGame("Account not created");
			GUI.clearTextInput();
		}
	}

	/**
	 * Used by command object to login
	 */
	public void methLogin() {
		String getUserName = (String) JOptionPane
				.showInputDialog(entry, "Enter your username:\n"
						+ "\"Press Cancel if you don't want to create one\"",
						"Username Enterer", JOptionPane.PLAIN_MESSAGE, null,
						null, null);
		String getPassword = (String) JOptionPane
				.showInputDialog(entry, "Enter your password:\n"
						+ "\"Press Cancel if you don't want to create one\"",
						"Password Enterer", JOptionPane.PLAIN_MESSAGE, null,
						null, null);
		String username = getUserName;
		String password = getPassword;
		GUI.clearTextInput();
		if (controller.getPlayerBase().checkPassword(username, password)) {
			GUI.setMyUserName(username);
//			GUI.setMe(controller.getPlayerBase().getPlayer(username));
			controller.getPlayerBase().getPlayer(username).setLoggedIn(true);
			GUI.addTextToGame("You have successfully logged in!");
		} else
			GUI.addTextToGame("Either your username or password is invalid");
	}

	/**
	 * Used by command object to say to players in the room this message.
	 */
	public void methSay() {
		command = argument.getArguments();
		System.out.println("My username is: " + command[0] + " My message is: "
				+ command[1]);
	}

	/**
	 * Used by the command object to speak all players in the game.
	 */
	public void methOOC() {
		command = argument.getArguments();
		System.out.println("My username is: " + command[0] + " My message is: "
				+ command[1]);
	}

	/**
	 * Used by the command object to whisper a specific person.
	 */
	public void methTell() {
		command = argument.getArguments();
		System.out.println("My username is: " + command[0] + " My message is: "
				+ command[1]);
	}

	/**
	 * Used by the command object to move a character north.
	 */
	public void methMoveNorth() {
		System.out.println("Hey I went North.");
	}

	/**
	 * Used by the command object to move a character south.
	 */
	public void methMoveSouth() {
		System.out.println("Hey I went South.");
	}

	/**
	 * Used by the command object to move a character east.
	 */
	public void methMoveEast() {
		System.out.println("Hey I went East.");
	}

	/**
	 * Used by the command object to move a character west.
	 */
	public void methMoveWest() {
		System.out.println("Hey I went West.");
	}

	/**
	 * Used by the command object to have a player quit the mud.
	 */
	public void methPlayerQuit() {
		System.out.println("Player has quit.");
	}

	/**
	 * Used by the command object to get the players inventory.
	 */
	public void methGetInventory() {
		System.out.println("This is what you got.");
	}

	/**
	 * Used by the command object to use an item.
	 */
	public void methUseItem() {
		command = argument.getArguments();
		System.out.println("My username is: " + command[0]
				+ " The item I used was: " + command[1]);
	}

	/**
	 * Used by the command object to drop a item.
	 */
	public void methDropItem() {
		command = argument.getArguments();
		System.out.println("My username is: " + command[0]
				+ " The item I dropped was: " + command[1]);
	}

	/**
	 * Used by the command object to get an item from the room.
	 */
	public void methGetItem() {
		// Need to write test
	}

	/**
	 * Used by the command object to get an object from a specific player.
	 * Requires permission from the player that is giving the object.
	 */
	public void methGetItemFromPlayer() {
		// Need to write test
	}

	/**
	 * Used by the command object to give an item to a specific person.
	 */
	public void methGiveItemToPlayer() {
		// Need to write test
	}

	/**
	 * Used by the command object to list all commands available.
	 */
	public void methListItems() {
		command = argument.getArguments();
		ArrayList<String> temp = (ArrayList<String>) command[0];
		System.out.println(temp.toString());
	}

	/**
	 * Used by the command object to list all parties logged in.
	 */
	public void methWho() {
		command = argument.getArguments();
		ArrayList<String> temp = (ArrayList<String>) command[0];
		System.out.println(temp.toString());
	}

	/**
	 * Used by the command object to list all the player's stats.
	 */
	public void methScore() {
//		String stats = GUI.getMe().statsToString();
//		GUI.addTextToGame(stats);
	}

	/**
	 * Used by the command object to list all commands available.
	 */
	public void methCommands() {
		command = argument.getArguments();
		Player me = (Player) command[0];
		controller.getCommands(me);
	}
}
