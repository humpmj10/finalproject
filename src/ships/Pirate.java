package ships;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import Galaxy.Planet;
import Items.Hull;
import Items.Item;
import Items.Shield;
import Items.ShieldSpecialist;
import Items.Weapon;
import Items.WeaponSpecialist;
import Model.CombatHandler;
import Model.Controller;

/**
 * The Pirate object is a common type of MOB. It initially tries to charge the
 * player a fee not to attack; if refused, the Pirate becomes hostile and
 * attacks. If the Pirate defeats the player, the player loses resources and/or
 * money.
 * 
 * @author Connor Lewis
 * 
 */
public class Pirate extends ActingMob implements Serializable {

	private int lvl;

	/**
	 * Constructor for the Pirate type of MOB.
	 * 
	 * @param title
	 *            the name of the pirate ship
	 * @param route
	 *            the ship's flight path
	 */
	public Pirate(String title, int level, int[] route) {
		// the MOB stopover at each planet is randomly calculated to be between
		// 45 and 60 seconds (not final)
		super(title, route, 60000 - (new Random()).nextInt(15001));

		this.setAwareness(30);
		lvl = level;

		this.setStats(new Hull(level), new Weapon(level), new Shield(level));
		
		if(lvl >= 4) {
			this.setLoot(new Item[] { new WeaponSpecialist()} );
		}
	}

	/**
	 * The pirate is automatically hostile and will attack the player, given to
	 * chance.
	 */
	@Override
	protected void perform(Player player) {

		Controller.getInstance().processCombat(this, player);

	}

	/**
	 * Any player attempt at friendly comms will result in the pirate attacking
	 * the player.
	 */
	@Override
	public void respond(Player player) {

		Controller.getInstance().processCombat(this, player);
	}

	/**
	 * 
	 */
	@Override
	public String toString() {

		return "The pirate ship " + name + " haunts this planet's skies.";
	}

	/**
	 * Returns a description of the pirate ship.
	 */
	@Override
	public String getDescription() {

		return name
				+ " is a level "
				+ lvl
				+ " pirate ship that is inherently hostile. It will attack you if it detects you or you try to engage it in friendly interaction.";
	}
}