package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import ships.Player;
import Commands.CommandReceiver;
import Model.Controller;
import Networking.Client;

/**
 * GUI class for our client object. Takes input from the user and broadcasts
 * that to the server.
 * 
 * @author Michael Humphrey and Brian Smith
 * 
 */
public class ClientGUI extends JFrame {

	public static final String HOST_NAME = "localhost";
	private Client client;
	private Thread startReader;

	private JTextField entry;
	private JTextArea response;
	private String myUserName;
	private boolean loggedIn;
	private boolean admin;
	private CommandReceiver cr;
	private Controller c;
	private Stack<String> commandsUndo = new Stack<String>();
	private Stack<String> commandsRedo = new Stack<String>();
	// private Player me;

	public static final int PORT_NUMBER = 4000;

	public static void main(String args[]) {
		ClientGUI clientGUI = new ClientGUI();
		clientGUI.setVisible(true);
		clientGUI.setSize(550, 650);
		clientGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public ClientGUI() {

		super();

		loggedIn = false;
		admin = false;
		entry = new JTextField();
		entry.setSize(200, 650);
		response = new JTextArea() {
			@Override
			public void append(String str) {
				super.append(str + "\n");
			}
		};

		// controller = Controller.getInstance();
		// controller.registerView(response);
		cr = cr.getHandle();
		cr.setEntry(entry);
		cr.setGUI(this);

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(entry, BorderLayout.SOUTH);
		getContentPane().add(response, BorderLayout.CENTER);

		// Register Listeners
		registerListeners();

		// Set up colors and font for game text
		response.setEditable(false);
		response.setBackground(Color.BLACK);
		response.setForeground(Color.GREEN);
		response.setLineWrap(true);
		response.setWrapStyleWord(true);
		response.setFont(new Font("Arial", 10, 16));
		setSize(600, 700);
		setVisible(true);

		// set up scroll bar
		JScrollPane vScrollBarGame = new JScrollPane(response);
		vScrollBarGame.setSize(50, 400);
		vScrollBarGame
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.add(vScrollBarGame);

		client = new Client();
		client.setWantToStayConnected(true);
		startReader();
		loadWelcome();
		c = c.getInstance();

	}

	/**
	 * 
	 */
	public void registerListeners() {
		// Register the chat listener
		ChatListener listener = new ChatListener();
		entry.addActionListener(listener);

		// Register the window listener
		CloseListener windowList = new CloseListener();
		this.addWindowListener(windowList);

		// Register the keylistener for the input history
		KeyList keys = new KeyList();
		entry.addKeyListener(keys);
	}

	/**
	 * Loads the welcome screen
	 */
	public void loadWelcome() {
		Scanner s = null;
		try {
			s = new Scanner(new File(baseDir + "Welcome" + ".txt"));
		} catch (FileNotFoundException e) {

			e.printStackTrace();
			System.out.println("File probably doesn't exist yet because lazy.");
		}
		while (s.hasNextLine()) {
			addTextToGame(s.nextLine());
		}
		s.close();
	}

	/**
	 * Method insures that file can be found regardless of the OS that this
	 * program is being ran on
	 */
	public static String baseDir = System.getProperty("user.dir")
			+ File.separator + "TextDocs" + File.separator;

	/**
	 * 
	 * @param myUserName
	 */
	public void setMyUserName(String myUserName) {
		this.myUserName = myUserName;
	}

	/**
	 * 
	 * @return True if player is logged in
	 */
	public boolean isLoggedIn() {
		return loggedIn;
	}

	/**
	 * Adds text to the game Text Area
	 */
	public void addTextToGame(String text) {
		response.append(text);
		response.setCaretPosition(response.getDocument().getLength());
	}

	/**
	 * Clear text from the input field
	 */
	public void clearTextInput() {
		entry.setText("");
	}

	/**
	 * Starts the reader in a separate thread to avoid freezing the GUI
	 */
	public void startReader() {
		IncomingReader reader = new IncomingReader();
		startReader = new Thread(reader);
		startReader.start();

	}

	public String getUserNameForMessage() {
		return myUserName + " says: ";
	}

	/**
	 * Private inner class that listens for input into the text field.
	 */
	private class ChatListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			commandsUndo.push(entry.getText());
			if (entry.getText().toLowerCase().startsWith("quit")) {

				client.setWantToStayConnected(false);
				client.quitCommand();
				System.exit(0);
			} else if (!loggedIn
					&& entry.getText().toLowerCase()
							.startsWith("createaccount")) {
				String username = (String) JOptionPane
						.showInputDialog(
								entry,
								"Enter your username:\n"
										+ "\"Press Cancel if you don't want to create one\"",
								"Username Picker", JOptionPane.PLAIN_MESSAGE,
								null, null, null);
				String password = (String) JOptionPane
						.showInputDialog(
								entry,
								"Enter your password:\n"
										+ "\"Press Cancel if you don't want to create one\"",
								"Password Picker", JOptionPane.PLAIN_MESSAGE,
								null, null, null);

				if (username != null && password != null & !username.equals("")
						& !password.equals("")) { // make sure an entry was
					// inputed and not blank
					client.createAccount(username, password);
					clearTextInput();

				} else { // player did not input or cancelled
					addTextToGame("Account not created, please enter valid information into Username/Password fields.");
					clearTextInput();
				}

			} else if (!loggedIn
					&& entry.getText().toLowerCase().startsWith("login")) {
				String getUserName = (String) JOptionPane
						.showInputDialog(
								entry,
								"Enter your username:\n"
										+ "\"Press Cancel if you don't want to create one\"",
								"Username Enterer", JOptionPane.PLAIN_MESSAGE,
								null, null, null);
				String getPassword = (String) JOptionPane
						.showInputDialog(
								entry,
								"Enter your password:\n"
										+ "\"Press Cancel if you don't want to create one\"",
								"Password Enterer", JOptionPane.PLAIN_MESSAGE,
								null, null, null);
				String username = getUserName;
				String password = getPassword.trim();
				clearTextInput();

				client.login(username, password);
				setMyUserName(username);

			} else if (entry.getText().equalsIgnoreCase("commands")) {
				getCommands();
			} else if (entry.getText().equalsIgnoreCase("GodMode")) {
				admin = true;
				addTextToGame("You have been granted admin privileges which allows you to shutdown the server");
			} else if (admin && entry.getText().equalsIgnoreCase("shutdown")) {
				client.shutDownServer();
			} else if (entry.getText().equalsIgnoreCase("who")) {
				client.getUsersLoggedIn();
			}
			// else if (isLoggedIn()
			// && entry.getText().equalsIgnoreCase("score")) {
			// c = c.getInstance();
			// c.getPlayerBase().getPlayer(myUserName).statsToString();
			// }
			else if (myUserName != null && isLoggedIn()
					&& entry.getText().toLowerCase().startsWith("ooc ")) {
				client.globalChat(getUserNameForMessage()
						+ entry.getText().substring(4));
			} else if (myUserName != null && isLoggedIn()
					&& entry.getText().toLowerCase().startsWith("say")) {
				client.planetChat(entry.getText().substring(4));

			} else if (myUserName != null && isLoggedIn()
					&& entry.getText().toLowerCase().startsWith("tell")) {
				Scanner keyboard = new Scanner(entry.getText());
				keyboard.next();
				String usernameTo = keyboard.next();
				String message = "";
				while (keyboard.hasNext()) {
					message += keyboard.next();
				}
				client.whisperChat(usernameTo, myUserName, message);
			} else if (myUserName != null && isLoggedIn()) {
				client.sendCommand(myUserName, entry.getText());
			} else if (myUserName == null) {
				addTextToGame("You need to login first");
			} else {
				addTextToGame(("\"" + entry.getText() + "\" is not a valid command!"));
			}
			clearTextInput();
		}
	}

	/**
	 * Used to listen to window closing, makes sure the connections are closed
	 * when the window is closed.
	 * 
	 * @author Michael Humphrey and Brian Smith
	 * 
	 */
	private class CloseListener extends WindowAdapter {

		@Override
		public void windowClosing(WindowEvent arg0) {
			client.setWantToStayConnected(false);
			client.quitCommand();
		}

	}

	/**
	 * Needed so that the GUI doesn't freeze up when waiting for connections
	 * It's a private class that starts in a new the thread.
	 * 
	 * @author Michael Humphrey
	 * 
	 */
	private class IncomingReader implements Runnable {

		public void run() {

			while (client.getWantToStayConnected()) {
				String message = "";
				try {
					message = (String) client.getInputFromLiasonLoop()
							.readObject();
					if (message.startsWith("/l ")) {
						String parsedMessage = message.substring(3);
						if (parsedMessage.equals("true")) {
							loggedIn = true;
							addTextToGame("You have successfully logged in!");
						} else {
							addTextToGame("Please enter a valid user name and password.");
						}
					} else {
						addTextToGame("\n" + message);
					}
				} catch (Exception e) {
					break;
				}
			}

		}
	}

	/**
	 * Private class to handle up/down inputs and recall commands.
	 */
	private class KeyList extends KeyAdapter {

		@Override
		public void keyPressed(KeyEvent e) {
			int keyCode = e.getKeyCode();
			switch (keyCode) {
			case KeyEvent.VK_UP:
				if (!commandsUndo.isEmpty()) {
					String undo = commandsUndo.pop();
					entry.setText(undo);
					commandsRedo.push(undo);
				}
				break;
			case KeyEvent.VK_DOWN:
				if (!commandsRedo.isEmpty()) {
					String redo = commandsRedo.pop();
					entry.setText(redo);
					commandsUndo.push(redo);
				}
				break;
			}
		}
	}

	/**
	 * Reads and appends the file for commands to the text area
	 */
	public void getCommands() {
		String commands = "\n" + "Here is a list of the commands: \nlook - " +
				"get description of surroundings	\nlook at <item or ship>" +
				" - get description of item or ship \ncommands - lists all " +
				"the commands useable by the you \nooc <message> (the basic " +
				"MUD wide chat command message goes to everyone currently connected) \nwho - (lists all players that are logged in)	\nsay (sends a message to all players in the same room as me) \ntell <player> <message> (sends a message to only the player targeted) \nscore (displays my current status/information) \ngive <item> to <target> (offers to give an item in my inventory to	player or sells it to a merchant NPC) \nget <item> (gets me an item from room) \nget <item> from <target> (gets me an item from target player/MOB/item, with their permission) \ninventory - lists the items that you are carrying \ndrop <item> - drops an item from my inventory to the room \nuse <item> - executes the item�s default behavior \nquit -  quits out of your game \nmove <direction> - north, south, east, west are available \nattack <ship> - initiates combat w/ ship \nengage <ship> - initiates non-hostile interaction w/ ship \nGodMode- grants you admin privileges \nUpArrow - displays the next command in the list of commands that have been entered	\nDownArrow - displays the previous command in the list of commands that have been entered \nshutdown - if you have admin on you can shutdown the server remotely";
		addTextToGame(commands);
	}
}