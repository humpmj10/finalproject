package Networking;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import ships.Player;

public class Client {
	private boolean wantToStayConnected;

	private ObjectOutputStream outputToLiasonLoop; // stream to server
	private ObjectInputStream inputFromLiasonLoop; // stream from server

	public static final int PORT_NUMBER = 4000;
	public static final String HOST_NAME = "150.135.1.44";

	public Client() {
		connectToServer();
	}

	/**
	 * Connects client to the server
	 */
	public void connectToServer() {
		Socket sock = null;
		try {
			// connect to the server
			sock = new Socket(HOST_NAME, PORT_NUMBER);
		} catch (Exception e) {
			System.out.println("Client was unable to connect to server");
			e.printStackTrace();
		}
		try {
			outputToLiasonLoop = new ObjectOutputStream(sock.getOutputStream());
			inputFromLiasonLoop = new ObjectInputStream(sock.getInputStream());

		} catch (Exception e) {
			System.out
					.println("Unable to obtain Input/Output streams from Socket");
			e.printStackTrace();
		}
	}

	/**
	 * Get input stream
	 * 
	 * @return ObjectInputStream
	 */
	public ObjectInputStream getInputFromLiasonLoop() {
		return inputFromLiasonLoop;
	}

	/**
	 * Sends a global chat request to the server
	 * 
	 * @param String
	 *            message to be sent
	 */
	public void globalChat(String message) {
		try {
			outputToLiasonLoop.writeObject("ooc " + message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends a quit command to the server.
	 */
	public void quitCommand() {
		try {
			outputToLiasonLoop.writeObject("quit");
			outputToLiasonLoop.close();
			inputFromLiasonLoop.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.exit(0);
	}

	/**
	 * Chat with players only near the planet
	 */
	public void planetChat(String message) {
		try {
			outputToLiasonLoop.writeObject("say " + message);
			// TODO Auto-generated need to implement this //
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends message to the server to shutdown
	 */
	public void shutDownServer() {
		try {
			outputToLiasonLoop.writeObject("/shut/");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Sends the server a message meant for a specific user.
	 * 
	 * @param String
	 *            username
	 */
	public void whisperChat(String username, String myUserName, String message) {
		try {
			outputToLiasonLoop.writeObject("tell " + username.toLowerCase()
					+ " " + myUserName + " " + message.trim());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sets the parameter so that the client can disconnect.
	 * 
	 * @param wantToStayConnected
	 */
	public void setWantToStayConnected(boolean wantToStayConnected) {
		this.wantToStayConnected = wantToStayConnected;
	}

	/**
	 * Gets the boolean that tells if the client wants to stay connected.
	 * 
	 * @return boolean
	 */
	public boolean getWantToStayConnected() {
		return this.wantToStayConnected;
	}

	/**
	 * Sends the player command to the Controller, which is located on the
	 * Server.
	 */
	public void sendCommand(String username, String command) {
		try {
			outputToLiasonLoop.writeObject("/c " + username + "/:/" + command);
			// TODO Auto-generated need to implement this //
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends request to create account to Server.
	 */
	public void createAccount(String username, String password) {
		try {
			outputToLiasonLoop.writeObject("/a " + username + "/:/" + password);
			// TODO Auto-generated need to implement this //
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get users logged in
	 */
	public void getUsersLoggedIn() {
		try {
			outputToLiasonLoop.writeObject("/who/");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Logs player into Server. Returns true if successful login, false if
	 * invalid.
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public void login(String username, String password) {
		try {
			outputToLiasonLoop.writeObject("/l " + username + "/:/" + password);
			// TODO Auto-generated need to implement this //
		} catch (IOException e) {
			e.printStackTrace();
		}

		
	}
}
