package Items;

import java.io.Serializable;

/**
 * An Upgrade which, once installed, functions as a cloaking device. I.e., upon
 * entering a system, this reduces the odds that actingmobs will detect the
 * player.
 * 
 * @author Connor James Lewis
 * 
 */
public class Cloak extends Upgrade implements Serializable {

	public Cloak() {

		setName();
		setValue();
	}

	@Override
	protected void setName() {

		this.name = "Cloaking Device";

	}

	@Override
	protected void setValue() {

		this.value = 350;
	}

	@Override
	public String getDescription() {

		return "The Cloaking Device is a special upgrade that lowers chances of detection by hostile entities. This upgrade is worth 350 credits.";
	}

}
