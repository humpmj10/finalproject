package Commands;

/**
 * Command interface
 * 
 * @author Michael Humphrey
 * 
 */
public interface Command {
	void execute();
}
