package Networking;

import java.io.Serializable;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import ships.Player;

/**
 * This class holds a list of all user created accounts. The list will contain
 * the player saved object, which contains stats, inventory and ship status.
 * Allows users to be deleted or added.
 * 
 * @author Michael Humphrey
 * 
 */
public class ClientList implements Serializable {

	private ConcurrentHashMap<String, Player> players;

	public ClientList() {
		players = new ConcurrentHashMap<String, Player>();
	}

	/**
	 * This method allows a user to be added to the list of user who have
	 * created accounts. The list is a hashmap so duplicates are not allowed.
	 * 
	 * @param username
	 * @param player
	 * @return true if the user was able to be added to the List of users
	 */
	public synchronized boolean addUser(String username, Player player) {
		if (!players.containsKey(username.toLowerCase())) {
			players.put(username.toLowerCase(), player);
			return true;
		} else
			return false;
	}

	/**
	 * Method is used to get the list of player accounts.
	 * 
	 * @return ConcurrentHashMap players
	 */
	public synchronized ConcurrentHashMap<String, Player> getPlayers() {
		return players;
	}

	/**
	 * This method allows the deletion of a User, requires that the password be
	 * supplied for the deletion to occur.
	 * 
	 * @param username
	 * @param password
	 * @return true if the deletion was successful
	 */
	public synchronized boolean deleteUser(String username, String password) {
		if (playerExists(username) && checkPassword(username, password)) {
			players.remove(username);
			return true;
		} else
			return false;
	}

	/**
	 * 
	 * @param username
	 * @return
	 */
	public boolean playerExists(String username) {
		if (!players.isEmpty())
			return players.containsKey(username.toLowerCase());
		else
			return false;
	}

	/**
	 * Check to see if password is valid with username and password
	 * 
	 * @param username
	 * @param password
	 * @return True if the password is correct
	 */
	public boolean checkPassword(String username, String password) {
		if (players.containsKey(username.toLowerCase())) {
			if (players.get(username.toLowerCase()).getPassword()
					.equals(password)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get a list of all players in the userBase
	 */
	public Collection<Player> getPlayerList() {
		return players.values();
	}

	/**
	 * Get a player from the database with a username
	 * 
	 * @param username
	 * @return Player The requested player from the list
	 */
	public Player getPlayer(String username) {
		if (playerExists(username.toLowerCase())) {
			return players.get(username.toLowerCase());
		} else
			return null;
	}
}
