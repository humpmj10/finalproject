package Items;

import java.io.Serializable;

/**
 * The Weapon upgrade increases a ship's attack stat.
 * 
 */
public class Weapon extends TieredUpgrade implements Serializable {

	public Weapon(int itemTier) {
		super(itemTier);

		if (itemTier > 5) {
			this.tier = 5;
		} else if (itemTier < 1) {
			this.tier = 1;
		} else {
			this.tier = itemTier;
		}

		this.setName();
		this.setValue();
	}

	/**
	 * Automatically sets the weapon's name upon being given the tier.
	 */
	@Override
	protected void setName() {
		String[] tiers = { "Light Energy Blaster", "Lead Slug Gun",
				"Laser Cannon", "Heavy Debilitator", "Heavy Devastator" };
		this.name = tiers[this.tier - 1];
	}

	@Override
	protected void setValue() {
		int[] tiers = { 5, 10, 20, 30, 45 };
		this.value = tiers[this.tier - 1];
	}

	@Override
	public String getDescription() {
		String[] tiers = {
				"The Light Energy Blaster is the weakest and cheapest weapon a ship can equip.",
				"The Lead Slug Gun is slightly better than the Light Energy Blaster, but still ineffective against enemies with decent shields.",
				"The Laser Cannon is a mid-range weapon operating off pure energy, effective against enemies with decent or weak shields.",
				"The Heavy Debilitator is a quality weapon capable of ending a fight swiftly, second only to the Debilitator.",
				"The Heavy Devastator is the best weapon available, presenting a real threat to even the best-equipped enemies." };
		return tiers[this.tier - 1] + " This weapon is worth " + this.getValue() + " credits.";
	}

}