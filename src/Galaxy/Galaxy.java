package Galaxy;

/**
 * 
 * @author Connor Lewis
 * Collection of Planets
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import ships.*;

import Items.Artefact;
import Items.Drill;
import Items.Fuel;
import Items.Hull;
import Items.Item;
import Items.Metals;
import Items.Minerals;
import Items.Resource;
import Items.Shield;
import Items.ShieldSpecialist;
import Items.Weapon;
import Items.WeaponSpecialist;
import Networking.ClientList;

public class Galaxy implements Serializable {

	private ArrayList<Planet> universe;

	private static Galaxy instance = null;
	private boolean planetsLoaded;
	private boolean resourcesLoaded;
	private boolean debrisLoaded;

	private Galaxy() {
		planetsLoaded = false;
		resourcesLoaded = false;
		debrisLoaded = false;
		universe = new ArrayList<Planet>();
		createPlanets();
		loadDebris();
		loadResources();
	}

	private static String baseDir = System.getProperty("user.dir")
			+ File.separator + "TextDocs" + File.separator;

	public static Galaxy getInstance() {
		if (instance == null) {
			System.out.println(baseDir + "savedGalaxy");
			FileInputStream fis;
			try {
				fis = new FileInputStream(baseDir + "savedGalaxy");
				ObjectInputStream ois = new ObjectInputStream(fis);
				instance = (Galaxy) ois.readObject();
				ois.close();
				instance.setMobsMovingAfterLoad();
			} catch (FileNotFoundException e) {
				instance = new Galaxy();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return instance;
	}

	/**
	 * Saves the galaxy to a file.
	 */
	public void saveGalaxy() {

		FileOutputStream fos;
		try {
			fos = new FileOutputStream(baseDir + "savedGalaxy");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(Galaxy.getInstance());
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("File not found save");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("File not found save");
		}
	}

	public boolean getPlanetsLoaded() {
		return planetsLoaded;
	}

	public void setPlanetsLoaded(boolean bool) {
		planetsLoaded = bool;
	}

	public void setResourcesLoaded(boolean bool) {
		resourcesLoaded = bool;
	}
	
	public void setDebrisLoaded(boolean bool) {
		debrisLoaded = bool;
	}
	
	private synchronized void setMobsMovingAfterLoad() {
		for(Planet planet : universe) {
			for(Ship ship : planet.getAllShips()) {
				if(ship instanceof Mob) {
					((Mob) ship).setPath();
				}
			}
		}
	}
	
	/**
	 * Creates all 36 planets with some preliminary names, subject to change.
	 */
	private void createPlanets() {

		ArrayList<String> planetNames = new ArrayList<String>();
		planetNames.add("Tyrion");
		planetNames.add("The Ship's Graveyard");
		planetNames.add("Doose");
		planetNames.add("Marimar");
		planetNames.add("Venom");
		planetNames.add("Walhart");

		planetNames.add("Albion");
		planetNames.add("Baia");
		planetNames.add("Descoladores");
		planetNames.add("Monaco");
		planetNames.add("Eros");
		planetNames.add("Formic");

		planetNames.add("Giedi Prime");
		planetNames.add("Vulcan");
		planetNames.add("Salusa Secundus");
		planetNames.add("Arrakis");
		planetNames.add("Caladan");
		planetNames.add("Lusitania");

		planetNames.add("Daenerys"); 
		planetNames.add("Path");
		planetNames.add("Jupiter"); 
		planetNames.add("Rov");
		planetNames.add("Mars"); 
		planetNames.add("Sorelledolce"); 

		planetNames.add("Trondheim");
		planetNames.add("Zanzibar");
		planetNames.add("Mercury"); 
		planetNames.add("Pacifica"); 
		planetNames.add("Neptune"); 
		planetNames.add("Damar");

		planetNames.add("Charn");
		planetNames.add("Calaspia");
		planetNames.add("Glorantha");
		planetNames.add("Keltia");
		planetNames.add("Saturn"); //
		planetNames.add("Stannis"); //

		// create planets
		for (String name : planetNames) {
			universe.add(new Planet(name));
		}

		// create connections between planets, they are laid out as a 6x6 grid
		// origin is upper left ... could add walls here too.
		int colCount = 6;
		for (int row = 0; row < 6; ++row) {
			for (int col = 0; col < 6; ++col) {
				int idx = row * colCount + col;
				Planet planet = universe.get(idx);

				// north
				int r = row - 1;
				int c = col;
				if (r >= 0) {
					int neighborIdx = r * colCount + c;
					planet.setNeighbor(Direction.NORTH,
							universe.get(neighborIdx));
				}

				// east
				r = row;
				c = col + 1;
				if (c < 6) {
					int neighborIdx = r * colCount + c;
					planet.setNeighbor(Direction.EAST,
							universe.get(neighborIdx));
				}

				// south
				r = row + 1;
				c = col;
				if (r < 6) {
					int neighborIdx = r * colCount + c;
					planet.setNeighbor(Direction.SOUTH,
							universe.get(neighborIdx));
				}

				// west
				r = row;
				c = col - 1;
				if (c >= 0) {
					int neighborIdx = r * colCount + c;
					planet.setNeighbor(Direction.WEST,
							universe.get(neighborIdx));
				}
			}
		}
	}

	public ArrayList<Planet> getUniverse()
	{
		return universe;
	}
	
	public Planet getAdjacentPlanet(Direction direction, Planet currentPlanet) {
		return currentPlanet.getNeighbor(direction);
	}

	public Planet planetForName(String name) {
		for (Planet p : universe) {
			if (p.getName().equals(name)) {
				return p;
			}
		}

		return null;
	}

	public Planet getPlanetByIndex(int i) {

		return universe.get(i);
	}

	public ArrayList<Planet> getPlanetsFromIndices(int[] list) {

		ArrayList<Planet> planets = new ArrayList<Planet>();
		for (int i : list) {
			planets.add(this.getPlanetByIndex(i));
		}

		return planets;

	}
	
	/**
	 * Called in constructor. Puts all items in their debris field at each of
	 * the relevant planets. Outer planets have better items in their debris
	 * than inner planets. NB: not every planet has stuff in its debris
	 * initially.
	 */
	private void loadDebris() {

		// corner planets
		addToPlanetDebris(0, new Item[] {
				new Fuel(),
				new Shield(5),
				new Artefact("Unsightly Protrusion",
						"\"Ribbed for her pleasure!\"") });
		addToPlanetDebris(
				5,
				new Item[] {
						new Weapon(5),
						new Shield(5),
						new Artefact(
								"Petrified Snout",
								"This petrified trunk of an extinct mammoth-like creature came from a distant planet.") });
		addToPlanetDebris(
				30,
				new Item[] {
						new Weapon(5),
						new Hull(5),
						new Artefact("Arc Fragment",
								"This artefact is a fragment of the first human spaceship's hull.") });
		addToPlanetDebris(
				35,
				new Item[] {
						new Fuel(),
						new Hull(5),
						new Artefact("Colossal Bone",
								"Found in an abandoned excavation on a nearby planet.") });
		addToPlanetDebris(
				12,
				new Item[] {
						new Weapon(2),
						new Hull(1),
						new Artefact("Smallish Bone",
								"Found in an abandoned excavation on a far off planet.") });
		addToPlanetDebris(30, new Item[] { new Fuel(), new Weapon(2),
				new Hull(3), });
		addToPlanetDebris(2, new Item[] { new Fuel(), new Weapon(2) });
		addToPlanetDebris(18, new Item[] { new Shield(2), new Weapon(2) });
		addToPlanetDebris(28, new Item[] { new Fuel(), new Weapon(3),
				new Drill() });
		addToPlanetDebris(28, new Item[] { new Fuel(), new Weapon(3),
				new Drill() });
		addToPlanetDebris(32,
				new Item[] { new WeaponSpecialist(), new Drill() });
		addToPlanetDebris(17,
				new Item[] { new ShieldSpecialist(), new Drill() });
		addToPlanetDebris(
				7,
				new Item[] {
						new Artefact("Residue",
								"A smelly yet valuable alien goop someone found in the freezer."),
						new Drill() });
		
		setDebrisLoaded(true);
	}

	/**
	 * Called in constructor. Places 2 - 4 resources at each planet.
	 */
	private void loadResources() {

		addToPlanetResources(0, new Resource[] { new Minerals(5),
				new Minerals(4), new Metals(5), new Metals(4) });
		addToPlanetResources(1, new Resource[] { new Minerals(5),
				new Minerals(3), new Metals(5), new Metals(4) });
		addToPlanetResources(2, new Resource[] { new Minerals(3),
				new Minerals(5) });
		addToPlanetResources(3, new Resource[] { new Metals(3),
				new Minerals(5), new Metals(5) });
		addToPlanetResources(4, new Resource[] { new Metals(4), new Metals(5) });
		addToPlanetResources(5, new Resource[] { new Minerals(3),
				new Minerals(5), new Metals(5) });

		addToPlanetResources(6, new Resource[] { new Minerals(3),
				new Minerals(5), new Metals(4) });
		addToPlanetResources(7, new Resource[] { new Minerals(3),
				new Minerals(5), new Minerals(4) });
		addToPlanetResources(8, new Resource[] { new Minerals(5),
				new Minerals(4), new Metals(3) });

		addToPlanetResources(9, new Resource[] { new Minerals(3),
				new Minerals(2), new Metals(4), new Metals(3) });
		addToPlanetResources(10, new Resource[] { new Minerals(4),
				new Minerals(5), new Minerals(4) });
		addToPlanetResources(11, new Resource[] { new Minerals(1),
				new Minerals(2), new Metals(1) });
		addToPlanetResources(12, new Resource[] { new Metals(5), new Metals(4),
				new Minerals(4), new Minerals(5) });

		addToPlanetResources(13, new Resource[] { new Minerals(3),
				new Metals(3) });
		addToPlanetResources(14, new Resource[] { new Minerals(2),
				new Metals(1), new Metals(1) });
		addToPlanetResources(15, new Resource[] { new Minerals(1),
				new Metals(2), new Minerals(1) });
		addToPlanetResources(16, new Resource[] { new Minerals(2),
				new Metals(2), new Minerals(1) });
		addToPlanetResources(17, new Resource[] { new Minerals(4),
				new Metals(5), new Minerals(4) });
		addToPlanetResources(18, new Resource[] { new Minerals(5),
				new Metals(5), new Minerals(4), new Minerals(4) });

		addToPlanetResources(19, new Resource[] { new Minerals(3),
				new Metals(3) });
		addToPlanetResources(20, new Resource[] { new Minerals(1),
				new Metals(1), new Metals(1) });
		addToPlanetResources(21, new Resource[] { new Minerals(1),
				new Metals(2), new Minerals(1) });
		addToPlanetResources(22, new Resource[] { new Minerals(2),
				new Metals(2), new Minerals(1) });
		addToPlanetResources(23, new Resource[] { new Minerals(4),
				new Metals(5), new Minerals(4) });
		addToPlanetResources(24, new Resource[] { new Minerals(5),
				new Metals(5), new Minerals(4), new Minerals(4) });
		addToPlanetResources(25, new Resource[] { new Minerals(2),
				new Metals(3), new Minerals(3), new Minerals(1) });
		addToPlanetResources(26, new Resource[] { new Minerals(2),
				new Metals(1), new Minerals(3), new Minerals(2) });
		addToPlanetResources(27, new Resource[] { new Minerals(3),
				new Metals(2), new Minerals(1), new Minerals(2) });
		addToPlanetResources(28, new Resource[] { new Minerals(3),
				new Metals(2), new Minerals(3), new Minerals(3) });
		addToPlanetResources(29, new Resource[] { new Minerals(4),
				new Metals(3), new Minerals(5), new Minerals(4) });
		addToPlanetResources(30, new Resource[] { new Minerals(5),
				new Minerals(4), new Metals(5) });
		addToPlanetResources(31, new Resource[] { new Minerals(3),
				new Minerals(5), new Metals(5), new Minerals(4) });
		addToPlanetResources(32, new Resource[] { new Minerals(4),
				new Metals(5), new Minerals(4), new Minerals(3) });
		addToPlanetResources(33, new Resource[] { new Minerals(3),
				new Minerals(5), new Metals(4) });
		addToPlanetResources(34, new Resource[] { new Metals(5),
				new Minerals(5) });
		addToPlanetResources(35, new Resource[] { new Minerals(4),
				new Minerals(5) });
		
		setResourcesLoaded(true);
	}

	/**
	 * Helper method used to add items to each planet's debris field, called
	 * during game construction.
	 * 
	 * @param planet
	 * @param items
	 */
	private void addToPlanetDebris(int planetIndex, Item[] itemsToAdd) {

		Planet planet = universe.get(planetIndex);

		if (itemsToAdd.length == 0) {
			return;
		} else {

			for (Item item : itemsToAdd) {
				planet.addDebris(item);
			}
		}
	}

	/**
	 * Helper method used to add items to each planet's resources, called during
	 * game construction.
	 * 
	 * @param planet
	 * @param items
	 */
	private void addToPlanetResources(int planetIndex, Resource[] resourcesToAdd) {

		Planet planet = universe.get(planetIndex);

		if (resourcesToAdd.length == 0) {
			return;
		} else {

			for (Resource resource : resourcesToAdd) {
				planet.addResource(resource);
			}
		}
	}

}
