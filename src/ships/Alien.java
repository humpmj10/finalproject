package ships;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import Galaxy.Planet;
import Items.Hull;
import Items.Item;
import Items.Shield;
import Items.Weapon;
import Model.CombatHandler;
import Model.Controller;

/**
 * The Alien is a type of MOB most similar to the Pirate. It is automatically
 * hostile and will attack if it detects the player.
 * 
 * @author Connor James Lewis
 * 
 */
public class Alien extends ActingMob implements Serializable {

	private int lvl;

	public Alien(String title, int level, int[] route) {

		// the Alien's stopover at each planet is randomly calculated to be
		// between 45 and 60 seconds (not final)
		super(title, route, 60000 - (new Random()).nextInt(15001));

		this.setAwareness(35);
		lvl = level;

		this.setStats(new Hull(level), new Weapon(level), new Shield(level));
	}

	/**
	 * Will attack player if it detects.
	 */
	@Override
	protected void perform(Player player) {

		Controller.getInstance().processCombat(this, player);
	}

	/**
	 * Will attack in response to player attempts at comms.
	 */
	@Override
	public void respond(Player player) {

		Controller.getInstance().processCombat(this, player);
	}

	/**
	 * 
	 */
	@Override
	public String toString() {

		return "The alien vessel " + name + " hovers above "
				+ this.currentPlanet.getName() + ".";
	}

	/**
	 * Returns a description of the alien ship.
	 */
	@Override
	public String getDescription() {

		return name
				+ " is a level "
				+ lvl
				+ " alien vessel of unknown origin. It distrusts humans and will "
				+ "usually attack on sight, or in response to human attempts at "
				+ "communication.";
	}
}