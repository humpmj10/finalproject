package Networking;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import Galaxy.Galaxy;
import Model.Controller;

import ships.Player;

/**
 * Server class that waits for incoming connections, each new connection is
 * added to a new thread that will handle only that client.
 * 
 * @author Michael Humphrey, Brian Smith, Brent Murdock and Connor Lewis
 * 
 */
public class Server implements Runnable {

	private ServerSocket myServerSocket;
	private Vector<Liason> playersConnected; // Vector used because arrayList is not thread safe
	private ConcurrentHashMap<Player, Liason> liasonsByPlayer;
	private final static int PORT_NUMBER = 4000;
	private Controller c;
	private Server server;


	public static void main(String args[]) {
		Server server = new Server();
		Thread serverThread = new Thread(server);
		serverThread.start();
	}

	public Server() {

		
		this.playersConnected = new Vector<Liason>();
		c = Controller.getInstance();
		c.loadPlayerBase();
		server = this;
		try {
			myServerSocket = new ServerSocket(PORT_NUMBER);
		} catch (IOException e1) {
		}

		liasonsByPlayer = new ConcurrentHashMap<Player, Liason>();
		c.setLiasonMap(liasonsByPlayer);
	}

	@Override
	public void run() {
		try {
			while (true) {
				// accept blocks until request comes over socket
				Socket intoServer = myServerSocket.accept();
				System.out.println("Is waiting for connect" + intoServer);
				// For every new connection, start a new Thread that will
				// communicate with that client. Each one will have access
				// to the common collection of all users who are connected.
				Liason aThreadForOneClient = new Liason(intoServer,
						playersConnected, liasonsByPlayer, server);
				Thread thread = new Thread(aThreadForOneClient);
				playersConnected.add(aThreadForOneClient);
				// always call the start() method on a Thread.
				thread.start();
			}
		} catch (IOException e) {
			c.savePlayerBase();

		}
	}
	
	/**
	 * Shut the server down
	 */
	public void exit() {
		System.out.println("Exiting the server!");
		Galaxy.getInstance().saveGalaxy();
		System.exit(0);
	}

}
