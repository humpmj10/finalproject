This dwarf planet lies at the edge of the Galaxy. Though small and distant, 
it houses some of the Galaxy's greatest minds, and is home to one of its wealthiest
governments. It is home to a burgeoning business district as well; what it lacks 
in natural resources, it triumphs in trade; those with money are well rewarded here.
The weather is warm and mild, and only the polar regions regularly freeze.

What appears on the surface to be a paradise of luxury and weather houses some
more intriguing secrets, if one only looks deeper. There are some hidden shops to be found;
which are tax free, if one knows the password. The place is littered with spies as well,
and the airspace does not lack for pirates. Though it may seem that Tyrion is lax concerning
illegal activity, many of their pirates are spies themselves, and the planet has some hidden
incomes. Tyrion is more like to send pirates after you than become overtly hostile should
you displease them.

Those who seek trade, intrigue, and secret underground operations are not 
like to be disappointed here.	  

	