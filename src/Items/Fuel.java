package Items;

import java.io.Serializable;

public class Fuel extends Usable implements Serializable {

	public Fuel() {
		super();

		this.setValue();
		this.setName();
	}

	@Override
	public String getDescription() {

		return "The Fuel Tank is a usable item. If the player is attacked by a much more powerful enemy, the Fuel Tank is automatically expended in order to escape combat. It is worth " + this.getValue() + " credits.";
	}

	@Override
	protected void setName() {
		this.name = "Fuel Tank";

	}

	@Override
	protected void setValue() {
		this.value = 40;

	}

}
